#include "SzachownicaWindow.h"
#include "utils.h"


contour_t testContours = { cv::Point2f(0,250.0),
cv::Point2f(250.0,250.0),
cv::Point2f(250.0,50.0),
cv::Point2f(50.0,50.0) };


void SzachownicaWindow::set_flags_on_callback()
{
	_show_org_img = (_trackbars[0].value == 1) ? true : false;
	_show_3dContours = (_trackbars[1].value == 1) ? true : false;
	_show_2dContours = (_trackbars[2].value == 1) ? true : false;
	_show_Found = (_trackbars[3].value == 1) ? true : false;
}

SzachownicaWindow::SzachownicaWindow(Reactor& img) :
	MyWindow(img, "Szachownica",
		trackbar_vect_t({
		{ 0, 1, "original" },
		{ 0, 1, "3d exp" },
		{ 0, 1, "2d exp" },
		{ 0, 1, "found" },
	})),
	_img_szachy(img.getSize(), img.getType(),0),
	_show_org_img(false),
	_show_3dContours(false),
	_show_2dContours(false),
	_show_Found(false)
{
	P.setExpirienced2D(testContours);
	do_callback();
}


SzachownicaWindow::~SzachownicaWindow()
{
}

void SzachownicaWindow::do_callback()
{
	
	set_flags_on_callback();
	Reactor* _imgR = (Reactor *)_img;
	_imgR->reset();
	if (_show_org_img) {
		//_img->doCanny();
		//_img->getCanny(_img_szachy);
		//_img->getImage(_img_szachy);
		//_img_szachy = _imgR->doCanny();
		_img_szachy = _img->getImage();
	}
	else
		_img_szachy = Mat::zeros(_img_szachy.size(), _img_szachy.type());
	if (_show_3dContours) {
		//_img_szachy = _imgR->doHough();
		//P.draw3D(_img_szachy);
	}
	if (_show_2dContours) {

		//_img->doHough();
		//_img->houghShow(_name);
		//_img_szachy = _imgR->doHough();


		P.draw2D(_img_szachy);
	}

	if (_show_Found) {
		P.drawFound(_img_szachy);
	}
	
	imshow(MyWindow::_name, _img_szachy);
}

void SzachownicaWindow::onWindowsChanged()
{
	Szachownica::lines_t hought_lines;
	//_img->doHough(hought_lines);
	//szachownica.find_Chessboard(hought_lines);
	//cout << this << "SzachownicaWindow got signal from: " << endl;
	do_callback();
}
