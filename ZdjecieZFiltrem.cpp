#include "ZdjecieZFiltrem.h"

void ZdjecieZFiltrem::init() {
	_is_filtered = false;
	_d = { 2, cv::MORPH_RECT };
	_e = { 2, cv::MORPH_RECT };
	setElements();
};

void ZdjecieZFiltrem::setElements() {
	_element_d = cv::getStructuringElement(_d.type, cv::Size(2 * _d.size + 1, 2 * _d.size + 1),
		cv::Point(_d.size, _d.size));
	_element_e = cv::getStructuringElement(_e.type, cv::Size(2 * _e.size + 1, 2 * _e.size + 1),
		cv::Point(_e.size, _e.size));
}

void ZdjecieZFiltrem::setFilter(const Dilation_setings & d_, const Erosion_setings & e_)
{
	_is_filtered = false;
	_d = d_;
	_e = e_;
	setElements();
}

void ZdjecieZFiltrem::doOpenFilter()
{
	_filtered = __doOpenFilter(_src);
}

void ZdjecieZFiltrem::doCloseFilter()
{
	_filtered = __doCloseFilter(_src);
}

cv::Mat ZdjecieZFiltrem::__doFilter(const cv::Mat & src)
{
	return __doCloseFilter(__doOpenFilter(src));
}

cv::Mat ZdjecieZFiltrem::__doOpenFilter(const cv::Mat & src)
{
	cv::Mat dst;
	cv::dilate(src, dst, _element_d);
	cv::erode(dst, dst, _element_e);
	return dst;
}

cv::Mat ZdjecieZFiltrem::__doCloseFilter(const cv::Mat & src)
{
	cv::Mat dst;
	cv::erode(src, dst, _element_e);
	cv::dilate(dst, dst, _element_d);
	return dst;
}

const cv::Mat & ZdjecieZFiltrem::getFilterd()
{
	return _filtered;
}



void ZdjecieZFiltrem::doOpenCloseFilter()
{
}

void ZdjecieZFiltrem::filteredShow(std::string window_name)
{
	if (_is_filtered)
		cv::imshow(window_name, _filtered);
	else
		imgShow(window_name);
}

ZdjecieZFiltrem::ZdjecieZFiltrem(const ZdjecieZFiltrem & _zdjecieZFiltrem): Zdjecie(_zdjecieZFiltrem)
{

	_filtered    = _zdjecieZFiltrem._filtered.clone();
	_d           =_zdjecieZFiltrem._d;
	_e			 =_zdjecieZFiltrem._e;
	_element_e   =_zdjecieZFiltrem._element_e.clone();
	_element_d   =_zdjecieZFiltrem._element_d.clone();
	_is_filtered =	_zdjecieZFiltrem._is_filtered;

}

ZdjecieZFiltrem::~ZdjecieZFiltrem()
{
}
