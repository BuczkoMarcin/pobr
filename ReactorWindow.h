#pragma once
#include "QuadrupleWindow.h"
#include "Reactor.h"

class ReactorWindow :
	public QuadrupleWindow
{
protected:
	// Inherited via QuadrupleWindow
	virtual void onWindowsChanged() override;
	virtual void do_Quadruple_callback() override;
public:

	ReactorWindow(Reactor& img) :QuadrupleWindow(img, "Reactor", trackbar_vect_t({
		{ 0,   21,   "F size" },
		{ 10,  1000, "C th1" },
		{ 100, 1000, "C th2"},
		{ 4,   100,  "H r0" },
		{ 180, 360,  "H theta" },
		{ 150, 360,  "H th" },
		{ 50,  400,  "H minLength" },
		{ 10,  800,  "H maxGap" }
	}), true, "Reactor settings") {
	};


};

