#pragma once
#include "Zdjecie.h"
#include <memory>
#include <boost/signals2.hpp>
#include <boost/ref.hpp>


//#include <boost/signal.hpp>
//#include <boost/bind.hpp>
#include <iostream>

using namespace boost;
using namespace std;



class MyWindow: public signals2::trackable
{
protected:
	static void trackbar_callback(int value, void* userdata);
	typedef vector<MyWindow*> windows_t;
	typedef map<Zdjecie*, windows_t> all_windows_t;

	typedef boost::signals2::signal<void()> windowSignal_t;
	windowSignal_t  _signal;
	


	friend ostream& operator<<(ostream& os, all_windows_t& aw);
	friend ostream& operator<<(ostream& os, windows_t& aw);

	static all_windows_t all_windows;
	virtual void do_callback() = 0;


	

	virtual void onWindowsChanged() = 0;

	//signal<void()>    Sig_Zdjecie;
public:
	typedef struct { int value; const int max_value; const string name; } trackbar_t;
	typedef vector< trackbar_t > trackbar_vect_t;
	
protected:
	void unregister_window();
	void register_window();
	
	//!obiekt obrazu
	Zdjecie* _img;

	//!nazwa okienkaa
	string _name;

	//! zmienne do pask�w przes�wnych parametr�wt
	trackbar_vect_t _trackbars;

public:
	MyWindow(Zdjecie & img, const string & window_name, trackbar_vect_t & trackbars, 
		      const bool & external_trackbars = false, const string & trackbar_window_name = "Trackbars");

	virtual ~MyWindow();
};

