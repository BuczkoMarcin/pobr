# POBR  
6/6/2016 8:07:25 PM  
Projekt na zaliczenie POBR (Przetwarzanie cyfrowe obrazów) Z2015. Program rozpoznający figury geometryczne na szachownicy z wykorzystaniem opencv i śladowo biblioteki boost.  

prowadzący: prof. dr hab. Przemysław Rokita  
projekt: mgr inż. Julian Myrcha


## Informacje ogóle

Projekt napisany pod windows (x86) w Visual Studio 2015. Kod jest niechlujny. Działa tylko kompilacja "Release". W `docs/Marcin_Buczko_POBR_dokumentacja_koncowa.pdf` jest dokumentacja końcowa projektu opisująca działanie programu i algorytmy. 

## Wymagania

- Boost wersja 1.59
- opencv 2.4.9
- Visula Studio 2015

## Kroki do uruchomienia

instalacja:  

- boost_1_59_0-msvc-14.0-64.exe
- opencv-2.4.9.exe
- Dodanie do PATH odpowiedniej ścieżki opencv249: `D:\opencv249\build\x86\vc12\bin`
- W VS 2015 po otworzeniu projektu należy zmienić odpowiednio ścieżki:  
Debug-> Framework Properties->Configuration Properties->VC++ Directories->
	- Include Directories:  
	`D:\opencv249\build\include;D:\Program files\boost_1_59_0;`  
	- Library Directories:  
	`D:\opencv249\build\x86\vc12\lib;D:\Program files\boost_1_59_0\lib64-msvc-14.0;`  
	Debug-> Framework Properties->Configuration Properties->Linker->Input->
	- Additional Dependencies:  
	`opencv_core249.lib;opencv_highgui249.lib;opencv_objdetect249.lib;opencv_imgproc249.lib;opencv_video249.lib;opencv_calib3d249.lib;kernel32.lib;user32.lib;gdi32.lib;winspool.lib;comdlg32.lib;advapi32.lib;shell32.lib;ole32.lib;oleaut32.lib;uuid.lib;odbc32.lib; odbccp32.lib;%(AdditionalDependencies)` 
- Po kompilacji w VS w katalogu /bin powinien pojawić się plik .exe. W tym samym katalogu muszą też być zawarte obrazki do przetwarzania s1.jpg, s2.jpg itd. Odwołania do nich łatwo znaleźć w kodzie w main(). 
 
