#pragma once
#include "MaskReactor.h"
#include "contours_utils.h"




using namespace std;

class FiguryFinder :
	public MaskReactor
{

	masked_t _masked_F;
	contours_F_t _contours_F;

	void init();

	bool setMasked_F(const int& i, const cv::Mat& maskedF);
	bool setContour_F(const int& i, const contours_h_t& maskedF);


	bool is_index_masked_F_ok(const size_t& i) const {
		return i >= 0 && i < _masked_F.size();
	};
	bool is_index_contours_F_ok(const size_t& i) const
	{
		return i >= 0 && i < _contours_F.size();
	};

	// dla czegokolwiek 
	static void __findContours(const cv::Mat& src, contours_h_t& dst);

	friend void form_CvSeq_to_vector(CvSeq* src, contours_t& dst);

public:
	const contours_h_t _findContours(const int& i);
	const cv::Mat _doFiguraImg(const int& i) const;
	const cv::Mat _doFiguraImg(const int& i, const cv::Mat& back) const;
	const contours_F_t findContours();
	void resetMasked_F();
	const cv::Mat getMasked_F(const int& i) const;
	const contours_h_t getConture_F(const int& i) const;

	FiguryFinder(cv::Mat& src) : MaskReactor(src) { };
	FiguryFinder(const std::string& file_name) : MaskReactor(file_name) { };
};

