#pragma once



#include "opencv2\calib3d\calib3d.hpp"
#include "opencv2\flann\flann.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include "utils.h"

#include <boost/bind.hpp>
#include <boost/ref.hpp>

#include <algorithm>
#include <iostream>



using namespace cv;
using namespace std;

namespace pobr {

	RNG rng(12345);

	vector<Vec4i> prun_verticla_lines(const vector<Vec4i>& vlines);
	int partition_lines(const vector<Vec4i>& lines, map< int, vector<Vec4i> >& labeled_lines, const double& max_distance_, const double& max_apha_);
	void perspectiveTransform(const vector<Vec4i>& src_lines, vector<Vec4i>& dest_lines, InputArray m);


	void print(const line_ab& lab) {
		cout << " a: " << lab.a << " b: " << lab.b << " perf_vert: " << lab.perfect_vertical << " vert_x: " << lab.vert_x << endl;
	}

	void scaleYline(Vec4i& line, const double& scale_, const int& Yp) {

		line[1] = int(double(line[1] / scale_ + Yp*(scale_ - 1) / scale_));
		line[3] = int(double(line[3] / scale_ + Yp*(scale_ - 1) / scale_));
	}

	void scaleYlines(vector<Vec4i>& lines, const double& scale, const int& Yp) {
		for (unsigned i = 0; i < lines.size(); ++i)
			scaleYline(lines[i], scale, Yp);
	}

	void lines(Mat& mat, const vector<Vec4i>& lines, const Scalar& color, int thickness, int lineType) {
		for(auto& l : lines)
			line(mat, l, color, thickness, lineType);
	}

	void line(Mat& mat, const Vec4i& l, const Scalar& color, int a, int b) {
		line(mat, Point(l[0], l[1]), Point(l[2], l[3]), color, a, b);
	}

	void dot(Mat& mat, const Point p, const Scalar& color) {
		circle(mat, p, 2, color, 2);
	}

	void transform_ab(const Vec4i& line, line_ab& lineab) {
		double dx = line[2] - line[0];
		double dy = line[3] - line[1];
		if (dx == 0) {
			lineab.perfect_vertical = true;
			lineab.vert_x = line[2];
		}
		else {
			lineab.perfect_vertical = false;
			lineab.a = dy / dx;
			lineab.b = line[1] - line[0] * lineab.a;
		}
	};

	void transform_vec(const line_ab& l, Vec4i& v) {

		if (l.perfect_vertical) {
			v[0] = int(l.vert_x);
			v[1] = 0;
			v[2] = int(l.vert_x);
			v[3] = 200;
		}
		else {
			if (l.a <= 1 && l.a >= -1) {
				v[0] = 0;
				v[1] = int(l.b);
				v[2] = 200;
				v[3] = int(double(200 * l.a) + l.b);
			}
			else {
				v[0] = int(double(-l.b / l.a));
				v[1] = 0;
				v[2] = int(double((200 - l.b) / l.a));
				v[3] = 200;
			}
		}

	}

	bool is_verticla_board_line(const Vec4i& line, const int& alpha) {
		line_ab lineab;
		transform_ab(line, lineab);
		if (lineab.perfect_vertical)
			return true;
		if (atan(lineab.a) * 180 / CV_PI > 90 - alpha || atan(lineab.a) * 180 / CV_PI < -90 + alpha)
			return true;
		return false;
	}

	bool is_horizontal_board_line(const Vec4i& line, const double& alpha) {


		line_ab lineab;
		transform_ab(line, lineab);
		if (lineab.perfect_vertical)
			return false;
		if (atan(1 / lineab.a) * 180 / CV_PI > 90 - alpha || atan(1 / lineab.a) * 180 / CV_PI < -90 + alpha)
			return true;
		return false;

		return false;
	}

	void extend_line(Vec4i& line, const double& max_x_, const double& max_y_, const int& margin) {


		line_ab lineab;
		transform_ab(line, lineab);
		double max_x = max_x_ - margin;
		double max_y = max_y_ - margin;
		double zero = margin;


		if (lineab.perfect_vertical) {
			line[0] = line[2] = lineab.vert_x;
			line[1] = zero;
			line[3] = max_y;
			return;
		}
		else if (lineab.a == 0) {
			line[0] = zero;
			line[1] = lineab.b;
			line[2] = max_x;
			line[3] = lineab.b;
			return;
		}
		else {
			Vec4i line_y_max;
			Vec4i line_x_max;
			double a = lineab.a;
			double b = lineab.b;
			int found_points = 0;

			//dla y = y_max

			line_y_max[0] = double((max_y - b) / a);
			line_y_max[1] = max_y;

			if (line_y_max[0] >= 0 && line_y_max[0] <= max_x_) {
				line[2 * found_points] = line_y_max[0];
				line[2 * found_points + 1] = line_y_max[1];
				++found_points;
			};

			// dla y = 0

			line_y_max[2] = double((zero - b) / a);
			line_y_max[3] = zero;

			if (line_y_max[2] >= 0 && line_y_max[2] <= max_x_) {
				line[2 * found_points] = line_y_max[2];
				line[2 * found_points + 1] = line_y_max[3];
				++found_points;
				if (found_points >= 2)
					return;
			};




			//dla x= x_max

			line_x_max[0] = max_x;
			line_x_max[1] = max_x * a + b;

			if (line_x_max[1] >= 0 && line_x_max[1] <= max_y_) {
				line[2 * found_points] = line_x_max[0];
				line[2 * found_points + 1] = line_x_max[1];
				++found_points;
				if (found_points >= 2)
					return;
			};


			// dla x= 0

			line_x_max[2] = zero;
			line_x_max[3] = zero*a + b;

			if (line_x_max[3] >= 0 && line_x_max[3] <= max_y_) {
				line[2 * found_points] = line_x_max[2];
				line[2 * found_points + 1] = line_x_max[3];
				++found_points;
			};
			//if (found_points != 2)
			//cout << "ERROR!" << max_y << " " << max_x << " " << found_points << " " << a << " " << b << " " << line_y_max << ":" << line_x_max << " : " << line << endl;

		}
	}

	void extend_lines(vector<Vec4i>& lines, const double& max_x_, const double& max_y_, const int& margin) {
		for (auto& l : lines) {
			extend_line(l, max_x_, max_y_, margin);
		}
	}

	bool crosspoint(const line_ab& L, const line_ab& K, Point& crosspoint_) {


		double x;
		double y;

		if ((L.perfect_vertical && K.perfect_vertical) || (K.a == L.a && !(L.perfect_vertical || K.perfect_vertical))) {
			//cout << " false ";
			return false;
		};

		if (K.perfect_vertical)
		{
			x = K.vert_x;
			y = L.a*x + L.b;
		}
		else if (L.perfect_vertical)
		{
			x = L.vert_x;
			y = K.a*x + K.b;
		}
		else
		{
			x = (K.b - L.b) / (L.a - K.a);
			y = L.a*x + L.b;
		};

		crosspoint_.x = x;
		crosspoint_.y = y;

		return true;
	}

	bool crosspoint(const Vec4i& L, const Vec4i& K, Point& crosspoint_) {

		line_ab Lab;
		line_ab Kab;

		transform_ab(L, Lab);
		transform_ab(K, Kab);

		return crosspoint(Lab, Kab, crosspoint_);
	}

	//!porownywanie linii unormowanych (kat nachylenia + odleglosci od srodkow linii) 
	bool isEqual_v(const Vec4i& _l1, const Vec4i& _l2, double& distance_m, const int& max_distance_, const double& max_apha_)
	{
		Vec4i l1(_l1), l2(_l2);
		line_ab ab_1, ab_2;
		int max_distance = max_distance_;      // odleg�o�� 5 punkt�w
		double max_alpha = max_apha_ * CV_PI / 180; // odchylenie 1 stopie� * x
		distance_m = 0;
		double norm_;

		transform_ab(l1, ab_1);
		transform_ab(l2, ab_2);

		double _angle = abs(atan(ab_1.a) - atan(ab_2.a));
		Point p1((l1[0] + l1[2]) / 2, (l1[1] + l1[3]) / 2);
		Point p2((l2[0] + l2[2]) / 2, (l2[1] + l2[3]) / 2);

		if (_angle >= max_alpha || _angle >= CV_PI - max_alpha) {
			//cout << _angle << endl;
			return false;
		};
		//cout << _angle << endl;
		norm_ = norm(p1 - p2);
		distance_m = 100 / (norm_*norm_ + 1);
		if (norm_ > max_distance)
			return false;

		return true;

	}

	bool isEqual_p(const Point& p1, const Point& p2, double max_distance)
	{
		if (norm(p1 - p2) <= max_distance)
			return true;
		return false;
	}

	Point center(const Vec4i& line) {
		return Point((line[0] + line[2]) / 2, (line[1] + line[3]) / 2);
	}

	void transform_center_to(const Point&  p, Vec4i&  l) {

		Point l_c = center(l);
		Point tranformation(p.x - l_c.x, p.y - l_c.y);

		l[0] += tranformation.x;
		l[1] += tranformation.y;

		l[2] += tranformation.x;
		l[3] += tranformation.y;

	}

	Vec4i normal_line(const Vec4i& line) {

		Vec4i normal(line[1], -line[0], line[3], -line[2]);
		Point l_center = center(line);
		transform_center_to(l_center, normal);


		return normal;
	}

	//! bez k�t�w prostych mi�dzy liniami - ten przypadek nie jest rozpatrywany
	Vec4i average(const vector<Vec4i>& lines) {

		Vec4i ave_l(0, 0, 0, 0);

		if (lines.size() == 0)
			return ave_l;

		if (lines.size() == 1)
			return lines[0];

		Vec4i l(lines[0]);
		Point cross_p1(0, 0);
		Point cross_p2(0, 0);
		Vec4i normal_l1 = normal_line(l);
		Vec4i normal_l2(normal_l1);

		transform_center_to(Point(l[0], l[1]), normal_l1);
		transform_center_to(Point(l[2], l[3]), normal_l2);

		int lines_number = 0;
		for (auto l : lines)
			if (crosspoint(normal_l1, l, cross_p1) && crosspoint(normal_l2, l, cross_p2)) {
				ave_l[0] += cross_p1.x;
				ave_l[1] += cross_p1.y;
				ave_l[2] += cross_p2.x;
				ave_l[3] += cross_p2.y;
				++lines_number;
			}
		if (lines_number > 0) {
			ave_l[0] /= lines_number;
			ave_l[1] /= lines_number;
			ave_l[2] /= lines_number;
			ave_l[3] /= lines_number;
		}

		return ave_l;
	}

	//! �rednie ze wszystkich polabelowanych linii
	vector<Vec4i> average(map< int, vector<Vec4i> >& label_lines) {
		vector<Vec4i> avg_lines;
		for (auto& lines : label_lines) {
			Vec4i avg_line = average(lines.second);
			avg_lines.push_back(avg_line);
		}
		return avg_lines;
	}

	vector<Vec4i> group_average(const vector<Vec4i>& lines, const int& cols, const int& rows, const double& max_distance, const double& max_alpha) {

		vector<Vec4i> avg_lines;
		map< int, vector<Vec4i> >labeled_lines;
		int numberOfLines = partition_lines(lines, labeled_lines, max_distance, max_alpha);
		//cout << " p: in:" << lines.size() << "out :" << numberOfLines << endl;

		avg_lines = average(labeled_lines);
		extend_lines(avg_lines, cols, rows, 0);
		return avg_lines;
	}

	int partition_points(const vector<Point>& crosspoints, vector<int>& labels, double max_distance) {
		return cv::partition(crosspoints, labels, boost::bind(isEqual_p, _1, _2, max_distance));
	}

	//! grupuje linie ze wzgl�du na pasowanie do siebie (odleg�o�� �rodk�w i k�t)
	int partition_lines(const vector<Vec4i>& lines, map< int, vector<Vec4i> >& labeled_lines, const double& max_distance_, const double& max_apha_) {

		for (auto& elem : labeled_lines) {
			elem.second.clear();
		}
		labeled_lines.clear();

		vector<int> labels;
		double d;
		int numberOfLabels = cv::partition(lines, labels, boost::bind(isEqual_v, _1, _2, ref(d), (int)max_distance_, max_apha_));

		for (size_t i = 0; i < lines.size(); i++)
		{
			Vec4i n_line = lines[i];
			labeled_lines[labels[i]].push_back(n_line);
		};

		return numberOfLabels;
	}

	void sort_verticla_lines(vector<Vec4i>& vlines) {
		struct cmp {
			inline bool operator() (const Vec4i& a, const Vec4i& b)
			{
				int ax = a[1] > a[3] ? a[0] : a[2];
				int bx = b[1] > b[3] ? b[0] : b[2];
				return ax < bx;
			}
		};
		std::sort(vlines.begin(), vlines.end(), cmp());
	}

	void sort_horizontal_lines(vector<Vec4i>& hlines) {
		struct cmp {
			inline bool operator() (const Vec4i& a, const Vec4i& b)
			{
				int ay = a[0] > a[2] ? a[1] : a[3];
				int by = b[0] > b[2] ? b[1] : b[3];
				return ay < by;
			}
		};
		std::sort(hlines.begin(), hlines.end(), cmp());
	}

	map<int, Point> all_crosspoints(vector<Vec4i>& lines, Vec4i& l) {

		map<int, Point> cross_points;
		int i = 0;
		for (auto l1 : lines) {
			Point p;
			if (crosspoint(l1, l, p))
				cross_points[i] = p;
			++i;
		}
		return cross_points;
	}

	void find_all_corsspoints(const vector<Vec4i>& lines, vector<Point>& crosspoints, vector<pair<int, int>>&  lines_idexes) {
		line_ab lineab;
		std::vector<line_ab> lines_ab;
		Point crosspoint_;
		pair<int, int> i_j;
		for (size_t i = 0; i < lines.size(); i++)
		{
			transform_ab(lines[i], lineab);
			lines_ab.push_back(lineab);
		};

		for (size_t i = 0; i < lines.size(); i++)
		{
			for (size_t j = i; j < lines.size(); j++)
			{
				if (crosspoint(lines_ab[i], lines_ab[j], crosspoint_)) {
					i_j.first = i;
					i_j.second = j;
					crosspoints.push_back(crosspoint_);
					lines_idexes.push_back(i_j);
				};
			};
		};
	};

	Point average(const vector<Point>& points) {
		double x = 0.0;
		double y = 0.0;
		for (auto p : points) {
			x += p.x;
			y += p.y;
		}

		x /= points.size();
		y /= points.size();
		return Point(x, y);
	}

	//! zwraca linie kt�re wychodz� z jednego punktu zbiegu. Zwraca najwi�kszy taki podzbi�r linii. 
	vector<Vec4i> prun_verticla_lines(const vector<Vec4i>& vlines, const int& cols, const int& rows) {

		vector<Point> crosspoints_;
		vector<pair<int, int>>  lines_index;
		vector<int> labels;
		vector<Vec4i> vlines_pruned;
		map<int, int> labels_number;

		int max_label_numbers = 0;
		int max_label = 0;

		find_all_corsspoints(vlines, crosspoints_, lines_index);
		int labelsNumber = partition_points(crosspoints_, labels, 5);
		for (auto label : labels) {
			labels_number[label] ++;
			if (labels_number[label] > max_label_numbers) {
				max_label_numbers = labels_number[label];
				max_label = label;
			};
		}
		int i = 0;
		for (auto label : labels) {
			if (label == max_label) {
				int l1_i = lines_index[i].first;
				int l2_i = lines_index[i].second;
				if (std::find(vlines_pruned.begin(), vlines_pruned.end(), vlines[l1_i]) == vlines_pruned.end())
					vlines_pruned.push_back(vlines[l1_i]);
				if (std::find(vlines_pruned.begin(), vlines_pruned.end(), vlines[l2_i]) == vlines_pruned.end())
					vlines_pruned.push_back(vlines[l2_i]);
			}
			++i;
		};
		extend_lines(vlines_pruned, cols, rows);
		return vlines_pruned;
	};

	vector<Vec4i> vertical_lines_from_Vanishing(const Point& vp, const int& l_numer, const double& dx, const double& da, const int& cols, const int& rows) {
		Vec4i line(vp.x, vp.y, 0, rows);
		vector<Vec4i> lines;
		double x;
		for (int i = 0; i < l_numer; i++) {
			x = i * ((cols - da) / l_numer) + da / 2 + cols / (l_numer * 2);
			x += dx;
			line[2] = x;
			//cout << "vertical_9lines_from_Vanishing y:" << x << " line: " << line << endl;
			lines.push_back(line);
		}
		return lines;
	};

	vector<Vec4i> horizontal_lines_inPerspective(Mat& warpMatrix, const int& l_number, const Point& vp,
		const Point& lb, const Point& rb, const Point& ru, const Point& lu) {
		vector<Vec4i> horizontal_lines;

		int width = rb.x - lb.x;
		//cout << "width: " << width << " " << rb.x << " " << lb.x << endl;

		Point2f rect[4];
		rect[0] = lb;
		rect[1] = rb;
		rect[2] = Point2f(rb.x, rb.y - width);
		rect[3] = Point2f(rb.x - width, rb.y - width);

		Point2f rect_[4];
		rect_[0] = lb;
		rect_[1] = rb;
		rect_[2] = ru;
		rect_[3] = lu;

		double dy = double(width) / double(l_number - 1);
		double y;
		for (int i = 0; i < l_number; ++i) {
			y = lb.y - dy * i;
			//cout << "y: " << y << " " << dy << " " << width << " " << i << endl;
			Vec4i h_l(lb.x, y, lb.x + width, y);
			horizontal_lines.push_back(h_l);
			//line(src,h_l,Scalar(255,0,0));
		}

		warpMatrix = getPerspectiveTransform(rect, rect_);
		perspectiveTransform(horizontal_lines, horizontal_lines, warpMatrix);
		warpMatrix = getPerspectiveTransform(rect_, rect);

		return horizontal_lines;

	}

	vector<Vec4i> horizontal_lines_inPerspective(Mat& warpMatrix, const int& l_number, const Point& vp, const Point& lb, const Point& ru) {

		if (lb.y - ru.y > lb.y - vp.y)
			return vector<Vec4i>();

		Vec4i v_lr(vp.x, vp.y, ru.x, ru.y);
		Vec4i v_ll(vp.x, vp.y, lb.x, lb.y);
		Vec4i h_lu(ru.x, ru.y, ru.x + 100, ru.y);
		Vec4i h_lb(lb.x, lb.y, lb.x + 100, lb.y);

		Point rb;
		Point lu;
		crosspoint(v_lr, h_lb, rb);
		crosspoint(v_ll, h_lu, lu);


		return horizontal_lines_inPerspective(warpMatrix, l_number, vp, lb, rb, ru, lu);
	}

	Point VanishigP(vector<Vec4i>& vlines) {
		vector<Point> crosspoints_;
		vector<pair<int, int>>  lines_index;
		find_all_corsspoints(vlines, crosspoints_, lines_index);
		Point sredni = average(crosspoints_);

		struct cmp {
			Point sredni;
			cmp(Point sr) {
				sredni = sr;
			};
			inline bool operator() (const Point& a, const Point& b)
			{
				return norm(a - sredni) < norm(b - sredni);
			}
		};
		std::sort(crosspoints_.begin(), crosspoints_.end(), cmp(sredni));
		crosspoints_.erase(crosspoints_.begin() + crosspoints_.size() * 2 / 3, crosspoints_.end());

		if (crosspoints_.size() == 0)
			return sredni;
		return average(crosspoints_);
	}

	vector<Vec4i>  numebrOfEqualLines(const vector<Vec4i>& L1, const vector<Vec4i>& L2, double& distance_m, int& numebrOfEquals, const double& max_distance, const double& max_alpha) {

		vector<Vec4i> eqlines;
		numebrOfEquals = 0;
		double dist_m;
		distance_m = 0;
		bool found_eql;
		double best_dist;
		Vec4i best_eql;
		for (auto& l1 : L1) {
			found_eql = false;
			best_dist = 0;
			for (auto& l2 : L2) {
				if (isEqual_v(l1, l2, dist_m, max_distance, max_alpha)) {
					if (!found_eql || best_dist < dist_m) {
						best_dist = dist_m;
						best_eql = l2;
					}
					found_eql = true;
				};
			};
			if (found_eql) {
				distance_m += dist_m;
				numebrOfEquals++;
				eqlines.push_back(best_eql);
			}
			else {
				eqlines.push_back(l1);
			}


		};
		if (numebrOfEquals > 0)
			distance_m /= double(numebrOfEquals);
		return eqlines;
	}

	vector<Vec4i> find_chessboard_vectical_lines(vector<Vec4i>& vlines, const int& cols, const int& rows,
		const int& marginx_, const int& margina_, const int& stepx, const int& stepa) {
		if (vlines.size() == 0)
			return vector<Vec4i>();

		Point vp = VanishigP(vlines);
		vector< vector<Vec4i> > vertical_test_lines;
		vector< pair<int, double> > numberOfEqualL;
		for (int dx = -marginx_; dx <= marginx_; dx += stepx) {
			for (int da = -margina_; da <= margina_; da += stepa) {
				vector<Vec4i> test_lines = vertical_lines_from_Vanishing(vp, 9, dx, da, cols, rows);
				extend_lines(test_lines, cols, rows);
				vertical_test_lines.push_back(test_lines);
			}
		}
		int max_Equal = -1;
		int max_index = 0;
		int max_dopasowanie = 0; // czym wi�kszy tym leszpsze dopasowanie nalezy do (0,100)
		double dist_m;
		for (unsigned i = 0; i < vertical_test_lines.size(); ++i) {
			dist_m = 100;
			pair<int, double> item;
			numebrOfEqualLines(vertical_test_lines[i], vlines, item.second, item.first);
			numberOfEqualL.push_back(item);
			if (numberOfEqualL[i].first >= max_Equal) {
				if (numberOfEqualL[i].first == max_Equal) {
					if (numberOfEqualL[i].second > max_dopasowanie) {
						max_Equal = numberOfEqualL[i].first;
						max_index = i;
						max_dopasowanie = numberOfEqualL[i].second;
					}
				}
				else {
					max_Equal = numberOfEqualL[i].first;
					max_index = i;
					max_dopasowanie = numberOfEqualL[i].second;
				};
			}
		}
		//cout << " E: " << max_Equal << " i " << max_index << " d: " << max_dopasowanie << endl;
		return vertical_test_lines[max_index];
	}

	void perspectiveTransform(const vector<Vec4i>& src_lines, vector<Vec4i>& dest_lines, InputArray m) {
		vector<Point2f> src_lp;
		vector<Point2f> dest_lp;

		for (auto l : src_lines) {
			src_lp.push_back(Point2f(l[0], l[1]));
			src_lp.push_back(Point2f(l[2], l[3]));
		}
		perspectiveTransform(src_lp, dest_lp, m);
		dest_lines.clear();
		for (int i = 0; i < dest_lp.size(); i += 2)
			if (i + 1 < dest_lp.size())
				dest_lines.push_back(Vec4i(dest_lp[i].x, dest_lp[i].y,
					dest_lp[i + 1].x, dest_lp[i + 1].y));
	}

	vector<Vec4i> find_chessboard_horizontal_lines(vector<Vec4i>& hlines, const vector<Vec4i>& vlines, Mat& warpMatrix, const int& cols, const int& rows) {

		if (hlines.size() > 1 && vlines.size() > 1) {
			Point vp;
			if (vlines.size() > 1)
				crosspoint(vlines[0], vlines[1], vp);

			vector< vector<Vec4i> > horizontal_test_lines;
			vector< pair<int, double> > numberOfEqualL;
			vector< vector<Vec4i> >  Eql_i;
			Point ru;
			Point lb;
			Point lu;
			Point rb;

			for (int i = 0; i < hlines.size(); ++i) {
				for (int j = i + 1; j <= hlines.size(); ++j) {

					crosspoint(hlines[i], vlines[vlines.size() - 1], ru);
					crosspoint(hlines[j], vlines[0], lb);
					crosspoint(hlines[i], vlines[0], lu);
					crosspoint(hlines[j], vlines[vlines.size() - 1], rb);

					if (abs(ru.y - lb.y) < rows / 2)
						continue;
					vector<Vec4i> test_lines = horizontal_lines_inPerspective(warpMatrix, 11, vp, lb, rb, ru, lu);
					extend_lines(test_lines, cols, rows);
					horizontal_test_lines.push_back(test_lines);
				}
			}
			int max_Equal = -1;
			int max_index = 0;
			int max_dopasowanie = 0; // czym wi�kszy tym leszpsze dopasowanie nalezy do (0,100)
			double dist_m;
			for (int i = 0; i < horizontal_test_lines.size(); ++i) {
				pair<int, double> item;
				Eql_i.push_back(numebrOfEqualLines(horizontal_test_lines[i], hlines, item.second, item.first, 10, 2));
				//for (auto j : Eql_i) {
				//	EqualL[i].push_back(hlines);
				//}
				//EqualL.push_back(equlines);

				numberOfEqualL.push_back(item);
				if (numberOfEqualL[i].first >= max_Equal) {
					if (numberOfEqualL[i].first == max_Equal) {
						if (numberOfEqualL[i].second > max_dopasowanie) {
							max_Equal = numberOfEqualL[i].first;
							max_index = i;
							max_dopasowanie = numberOfEqualL[i].second;
						}
					}
					else {
						max_Equal = numberOfEqualL[i].first;
						max_index = i;
						max_dopasowanie = numberOfEqualL[i].second;
					};
				}
			}
			//cout << " E: " << max_Equal << " i " << max_index << " d: " << max_dopasowanie << endl;
			//for (auto l : horizontal_test_lines[max_index])
			//	line(src, l, Scalar(0, 50, 127), 2, 8);

			return Eql_i[max_index];
		}
		else {
			return vector<Vec4i>();
		}

	}

};
