#include "Zdjecie.h"
#include "utils.h"

ostream& operator<<(ostream& os, Hough_setings h) {
	os << "r0: " << h.hough_r0
		<< ", theta: " << h.hough_theta
		<< ", thr: " << h.hough_thr
		<< ", minLength: " << h.hough_minLength
		<< ", maxGap: " << h.hough_maxGap;
	return os;
};

bool is_part_of(const cv::Mat& mat, const cv::Rect& rect) {
	if (rect.x >= 0 && rect.x + rect.width >= 0 && mat.cols >= rect.x + rect.width &&
		rect.y >= 0 && rect.y + rect.height >= 0 && mat.rows >= rect.y + rect.height) {
		return true;
	}
	return false;
}

void push_back_to_vector(const cv::Mat& mat, lines_t& lines) {
	//cout << "mat.depth(): " << mat.depth() << " mat.total(): " << mat.total() << endl;
	if (mat.total() <= 0)
		return;
	
	CV_Assert(mat.depth() == 4);
	for (int i = 0; i < mat.rows; ++i)
		for (int j = 0; j < mat.cols; ++j) {
			lines.push_back(mat.at<Vec4i>(i, j));
		};
}


Zdjecie::Zdjecie(const Zdjecie & _zdjecie)
{
	 _src = _zdjecie._src.clone();
	 _canny = _zdjecie._canny.clone();
	 _hough = _zdjecie._hough.clone();
	 _c = _zdjecie._c;
	 _h = _zdjecie._h;
	 _hough_lines = _zdjecie._hough_lines;
	 //_zdjecie.getHough(_hough_lines);
	_is_canny = _zdjecie._is_canny;
	_is_hough = _zdjecie._is_hough;
	_is_hough_img_modified = _zdjecie._is_hough_img_modified;

}

Zdjecie::Zdjecie(const cv::Mat& src)
{
	_src = src.clone();
	Init();
}

Zdjecie::Zdjecie(const std::string& file_name)
{
	_src = cv::imread(file_name, 1);
	Init();
}

Zdjecie::~Zdjecie()
{
	//_hough_lines.clear();
}

cv::Mat Zdjecie::__doCanny(const cv::Mat & src)
{
	cv::Mat dst;
	try {
		cv::Canny(src, dst, _c.canny_thr1, _c.canny_thr2, 3);
	}
	catch (exception e) {
		std::cout << e.what() << endl;
	}
	return dst;
}

lines_t Zdjecie::__doHough(const cv::Mat & src)
{
	vector < cv::Vec4i > region_lines;
	try {
		cv::Mat m_t;
		cv::HoughLinesP(src, m_t, _h.hough_r0, CV_PI / _h.hough_theta, _h.hough_thr, _h.hough_minLength, _h.hough_maxGap);
		push_back_to_vector(m_t, region_lines);
	}
	catch (std::exception e) {
		std::cout << e.what() << std::endl;
	};
	return region_lines;
}

cv::Mat Zdjecie::__doHoughImg(const lines_t & lines, const cv::Mat& backgrond)
{
	CV_Assert(backgrond.channels() == 3);
	cv::Mat dst = backgrond.clone();
	cv::RNG rng(12345);
	cv::Scalar color;
	for (auto l : lines) {
		color = cv::Scalar(rng.uniform(0, 255), rng.uniform(0, 255), rng.uniform(0, 255));
		pobr::line(dst, l, color, 2);
	};
	return dst;
}

void Zdjecie::doHoughImg()
{
	if (_is_hough && _is_hough_img_modified) {
		_hough = _canny.clone();
		cv::cvtColor(_hough, _hough, CV_GRAY2BGR);
		_hough = __doHoughImg(_hough_lines, _hough);
		_is_hough_img_modified = false;
	}
}

void Zdjecie::Init() {
	_c = {50,200};
	_h = {4.0, CV_PI / 180.0, 150, 50.0, 10.0, 1, 1};
	_is_canny = false;
	_is_hough = false;
	_is_hough_img_modified = false;
}

void Zdjecie::doHough() {
	try {
		_doHough_in_squers(_h.x_factor, _h.y_factor);
	}
	catch (std::exception e) {
		std::cout << e.what() << std::endl;
	};
}

void Zdjecie::_doHough_in_squers(const unsigned& x_factor, const unsigned& y_factor)
{
	if (x_factor == 0 || y_factor == 0) return;

	if (!_is_canny)
		doCanny();
	if (!_is_hough) {
		const int len_x = _canny.cols / x_factor;
		const int len_y = _canny.rows / y_factor;
		int lx, ly;
		cv::Point p(0, 0);
		for (unsigned x = 0; x < x_factor; ++x) {
			lx = (p.x + len_x > _canny.cols) ? _canny.cols - p.x : len_x;
			for (unsigned y = 0; y < y_factor; ++y) {
				ly = (p.y + len_y > _canny.rows) ? _canny.rows - p.y : len_y;
				try {
					_doHough_in_squer(cv::Rect(p, Size(lx, ly)));
				}
				catch (std::exception e) {
					std::cout << e.what() << std::endl;
				};
				//cv::rectangle(_canny, cv::Rect(p, Size(lx, ly)), Scalar(255,0,0),1);
				p.y += ly - 1;
			}
			p.y = 0;
			p.x += lx - 1;
		};
		_is_hough_img_modified = true;
	};
	_is_hough = true;

}
//! wykonuje znajdowanie linii w okre�lonym regionie. Nie sprawdza warunk�w 
//czy by� wykonany canny (_is_canny) czy by� ju� wykonany hough (_is_hough)
void Zdjecie::_doHough_in_squer(const cv::Rect & region)
{
	if (!is_part_of(_canny, region))
		return;
	
	try {
		vector < cv::Vec4i > region_lines = __doHough(_canny(region));
		for (cv::Vec4i l : region_lines) {
			l[0] += region.x;
			l[1] += region.y;
			l[2] += region.x;
			l[3] += region.y;
			_hough_lines.push_back(l);
		};
	}
	catch (std::exception e) {
		std::cout << e.what() << std::endl;
	};

}

void Zdjecie::doCanny() {
	if (!_is_canny) 
		_canny = __doCanny(_src);
	_is_canny = true;
}

bool Zdjecie::getCanny(cv::Mat& dst) {
	if (_is_canny) {
		dst = _canny.clone();
		return true;
	};
	return false;
};

bool Zdjecie::getHough(lines_t& dst) const {
	if (_is_hough) {
		dst = _hough_lines;
		return true;
	};
	return false;
};

void Zdjecie::getImage(cv::Mat& dst) {
	dst = _src.clone();
}

cv::Mat  Zdjecie::getImage()
{
	return _src.clone();
};

void Zdjecie::setHough(const Hough_setings& h) {
	_h = h;
	_hough_lines.clear();
	_is_hough = false;
	_is_hough_img_modified = false;
};

void Zdjecie::setCanny(const Canny_setings& c) {
	_c = c;
	_is_canny = false;
	_is_hough = false;
	_is_hough_img_modified = false;
};

void Zdjecie::doCanny(cv::Mat& dst) {
	doCanny();
	getCanny(dst);
};

void Zdjecie::doHough(lines_t& dst) {
	doHough();
	getHough(dst);
};

void Zdjecie::imgShow(std::string window_name) {
	cv::imshow(window_name, _src);
}

void Zdjecie::cannyShow(std::string window_name) {
	if (_is_canny)
		cv::imshow(window_name, _canny);
	else
		imgShow(window_name);
}

void Zdjecie::houghShow(std::string window_name)
{
	if (_is_hough) {
		if(_is_hough_img_modified)
			doHoughImg();
		cv::imshow(window_name, _hough);
	}
	else
		imgShow(window_name);

}

cv::Size Zdjecie::getSize()
{
	return _src.size();
}

int Zdjecie::getType()
{
	return _src.type();
}





