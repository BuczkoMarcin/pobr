#include "segment_labels.h"


//! Ustala label za marginesami obrazu jako _bad_label. Pozosta�e jako _null_label  
Segment_labels::Segment_labels(const cv::Mat& picture) :
	_lenght_x(picture.cols + 2), _lenght_y(picture.rows + 2), _labels(picture.cols +2, std::vector<t_Label>(picture.rows + 2))
{
	for (int x = -1; x < _lenght_x - 1; ++x)
		for (int y = -1; y < _lenght_y - 1; ++y){
			if (x == -1 || y == -1 || x == _lenght_x - 2 || y == _lenght_y - 2)
				this->operator()(x, y) = _bad_label;
			else
				this->operator()(x, y) = _null_label;
	}	
}


Segment_labels::~Segment_labels()
{
	_labels.clear();
}

Segment_labels::t_Label& Segment_labels::operator() (const int& x, const int& y)
{
	
	return _labels[x+1][y+1];
}