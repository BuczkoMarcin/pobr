#include "Przedmiot2DWindowTest.h"
#include "utils.h"
#include "contours_utils.h"
#include "pobr_types.h"





void Przedmiot2DWindowTest::set_flags_on_callback()
{
	_show_org_img = (_trackbars[0].value == 1) ? true : false;
	_show_Defined = (_trackbars[1].value == 1) ? true : false;
	_show_2dContours = (_trackbars[2].value == 1) ? true : false;
	_show_Found = (_trackbars[3].value == 1) ? true : false;
	_angle = double(_trackbars[4].value) * CV_PI / 180.0;
	_x = _trackbars[5].value;
	_y = _trackbars[6].value;
	_mask_i = _trackbars[7].value;

}

Przedmiot2DWindowTest::Przedmiot2DWindowTest(FiguryFinder& img) :
	MyWindow(img, "Przedmiot2D",
		trackbar_vect_t({
		{ 0, 1, "original" },
		{ 0, 1, "defined" },
		{ 0, 1, "2d exp" },
		{ 0, 1, "found" },
		{ 0, 500, "angle" },
		{0, 300,"x move"},
		{ 0,300,"y move" },
		{ 0,3,"mask" },
})),
_img_szachy(img.getSize(), img.getType(), 0),
_show_org_img(false),
_show_Defined(false),
_show_2dContours(false),
_show_Found(false),
_x(0), _y(0), _angle(0), _mask_i(0), 
P(czerwonyKlocekProperties),
P1(testProperties),
P2(testProperties)
{
	do_callback();
	_img_F = (FiguryFinder*)_img;
}


Przedmiot2DWindowTest::~Przedmiot2DWindowTest()
{
}

void Przedmiot2DWindowTest::do_callback()
{

	set_flags_on_callback();
	Reactor* _imgR = (Reactor *)_img;
	_imgR->reset();
	if (_show_org_img) {
		//_img->doCanny();
		//_img->getCanny(_img_szachy);
		//_img->getImage(_img_szachy);
		//_img_szachy = _imgR->doCanny();
		_img_szachy = _img->getImage();
	}
	else
		_img_szachy = Mat::zeros(_img_szachy.size(), _img_szachy.type());
	if (_show_Defined) {
		//_img_szachy = _imgR->doHough();
		//P.draw3D(_img_szachy);
		P.drawDefined(_img_szachy);
	}
	if (_show_2dContours) {
		//_img->doHough();
		//_img->houghShow(_name);
		//_img_szachy = _imgR->doHough();
		contour2f_t cc = czerwonyKlocekContour;
		//contour2f_t cc = contour;
		
		//cv::Point2f center = cv::Point2f(pobr::rng.uniform(0, 255), pobr::rng.uniform(0, 255));
		cv::Point2f cm = centerOfMass(cc);
		rotate(_angle, cm, cc);
		
		cv::Point2f center(_x, _y);
		move(center, cc);
		
		//cv::Point2f center(_x, _y);
		
		P.setExpirienced2D(cc);
		P.draw2D(_img_szachy);


		//if (contour.size() > 0) {
		//	P.setExpirienced2D(contour);
		//	P.draw2D(_img_szachy);
		//}
		//
		//if (contour1.size() > 0) {
		//	P1.setExpirienced2D(contour1);
		//	P1.draw2D(_img_szachy);
		//}
		//if (contour2.size() > 0) {
		//	P2.setExpirienced2D(contour1);
		//	P2.draw2D(_img_szachy);
		//}
	}

	if (_show_Found) {
		P.findPrzedmiot();
		P.drawFound(_img_szachy);
		//P1.findPrzedmiot();
		//P1.drawFound(_img_szachy);
		//P2.findPrzedmiot();
		//P2.drawFound(_img_szachy);
	}

	imshow(MyWindow::_name, _img_szachy);
}

void Przedmiot2DWindowTest::onWindowsChanged()
{


	_img_F->resetMasked_F();
	contours_F_t cFT = _img_F->findContours();
	contour.clear();
	if (cFT.size() > 0 && cFT[0].contours.size() > 0)
		contour = contour2i_to_2f(cFT[0].contours[0]);
	//else;
	//	std::cout << "no contour" << endl;

	if (cFT.size() > 1 && cFT[1].contours.size() > 0)
		contour1 = contour2i_to_2f(cFT[1].contours[0]);
	if (cFT.size() > 2 && cFT[2].contours.size() > 0)
		contour2 = contour2i_to_2f(cFT[2].contours[0]);


	//Szachownica::lines_t hought_lines;
	//_img->doHough(hought_lines);
	//szachownica.setChessboardLines(hought_lines);
	//cout << this << "Przedmiot2DWindowTest got signal from: " << endl;
	do_callback();
}
