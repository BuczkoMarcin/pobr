
#pragma once
#include "Klocek.h"
class Walec :
	public Klocek
{
	void findDefinition();
public:
	Walec(const contour2f_t & contour3d_exp, const cv::Mat & matrix3DTo2D);
	Walec(const Przedmiot_properties & definition);
	Walec();
	~Walec();
	bool findPrzedmiot();
	void setExpirienced3D(const contour2f_t & contour_exp);
};