#pragma once
#include "Reactor.h"
#include "Mask.h"
#include "pobr_types.h"




class MaskReactor :
	public Reactor
{
public:

	
protected:
	masked_t _masked;

	// znalezione maski
	masks_t _masks;

	static const cv::Mat __doMask(const Mask& mask, const cv::Mat& src);
	
	inline bool is_index_masked_ok(const size_t& i) const { return i >= 0 && i < _masked.size(); };
	inline bool is_index_masks_ok(const size_t& i) const { return i >= 0 && i < _masks.size(); };

public:
	const cv::Mat _doMask(const int & i, const Mask & mask_);
	const cv::Mat _doMask(const int & i, const HSV_setings & mask_s);
	const cv::Mat _doMask(const int& i);
	const masked_t doMask();
	const masked_t doMask(const masks_t& masks);
	const masked_t doMask(const std::vector<HSV_setings>& masks_s);
	bool getMask(const int& i, Mask& dst);

	masked_t getMasked() { return _masked; };
	bool getMasked(const int& i, cv::Mat& dst);
	void setMasks(const masks_t& masks) { _masks = masks;  };

	MaskReactor(cv::Mat& src) : Reactor(src), _masked(), _masks() { };
	MaskReactor(const std::string& file_name) : Reactor(file_name), _masked(), _masks() { };

};

