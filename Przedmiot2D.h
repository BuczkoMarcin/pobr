#pragma once
#include "Przedmiot.hpp"
#include "pobr_types.h"

class Przedmiot2D :
	public Przedmiot
{
private:
	//znajd� orientacje obiektu na podstawie _definition i _contour2D_exp
	bool findRotation(double& rotation) const;

	// znajd� po�o�enie obiektu na podstawie _contour2D_exp
	bool findTranslation(cv::Point2f& translation) const;

public:



	static const Przedmiot_properties default_properties;

	// definicja obiektu  - tak jak powinien wygl�da� na p�aszczy�nie.
	Przedmiot_properties _definition;

	// contour obiektu jaki zosta� uzyskany w rzucie na p�aszczyzn� 2D.
	contour2f_t    _contour2D_exp;

	// rozpoznany obiekt - dopasowanie definicji do do�wiadczenie. 
	// contour z definicji obr�cony i umieszczony w odpowiednim miejscy. 
	contour2f_t    _contour_found;

	// czy obiekt jest rozpoznany
	bool		 _is_found;

	// czy obiekt ma contour uzyskany ze zdj�cia w rzucie na p�aszczyzn� 2d
	bool		 _is_expirienced2D;
protected:

	void reset();
	Przedmiot2D();

	// ustawia definicje obiektu
	void setDefinition(const Przedmiot_properties& definition);

	bool is_expirienced2D() {return _is_expirienced2D;};
	bool is_found() {return _is_found;};
public:
	// znajduje kontury przedmiotu na podstawie _definition i _contour2D_exp
	bool findPrzedmiot(contour2f_t& contour_found);
	bool findPrzedmiot();
	
	// ustawia contour uzyskany ze zdj�cia w rzucie 2D
	void setExpirienced2D(const contour2f_t&  contour_exp);

	// zwraca contour uzyskany ze zdj�cia w rzucie 2D
	bool getExpirienced2D(contour2f_t&  contour_exp);

	// zwraca definicje obiektu w rzucie 2D
	bool getDefinition(Przedmiot_properties& definition);


	// zwraca contour uzyskany ze zdj�cia w rzucie 2D
	bool getFound(contour2f_t&  contour_found);

	// przeskalowuje definicje
	void scaleDefinition(const double& scale);


	void drawDefined(cv::Mat& backgroung,const cv::Scalar& color = cv::Scalar(127, 255, 127), const int& thickness = 1) const;
	void draw2D(cv::Mat& backgroung) const;
	void drawFound(cv::Mat& backgroung, const cv::Scalar& color = cv::Scalar(127, 255, 127), const int& thickness = 1) const;
	Przedmiot2D(const Przedmiot_properties& definition);
};

