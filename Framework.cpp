#include "opencv2/core/core.hpp"
#include "opencv2/highgui/highgui.hpp"
#include <iostream>
#include <list>
#include <map>


cv::Mat& perform(cv::Mat& I) {
	CV_Assert(I.depth() != sizeof(uchar));
	switch (I.channels()) {
	case 1:
		for (int i = 0; i < I.rows; ++i)
			for (int j = 0; j < I.cols; ++j)
				I.at<uchar>(i, j) = (I.at<uchar>(i, j) / 32) * 32;
		break;
	case 3:
		cv::Mat_<cv::Vec3b> _I = I;
		for (int i = 0; i < I.rows; ++i)
			for (int j = 0; j < I.cols; ++j) {
				_I(i, j)[0] = (_I(i, j)[0] / 32) * 32;
				_I(i, j)[1] = (_I(i, j)[1] / 32) * 32;
				_I(i, j)[2] = (_I(i, j)[2] / 32) * 32;
			}
		I = _I;
		break;
	}
	return I;
}




cv::Mat selectMax(cv::Mat& I) {
	CV_Assert(I.depth() != sizeof(uchar));
	cv::Mat  res(I.rows, I.cols, CV_8UC3);
	switch (I.channels()) {
	case 3:
		cv::Mat_<cv::Vec3b> _I = I;
		cv::Mat_<cv::Vec3b> _R = res;
		for (int i = 0; i < I.rows; ++i)
			for (int j = 0; j < I.cols; ++j) {
				int sel = (_I(i, j)[0] < _I(i, j)[1]) ? 1 : 0;
				sel = _I(i, j)[sel] < _I(i, j)[2] ? 2 : sel;
				_R(i, j)[0] = sel == 0 ? 255 : 0;
				_R(i, j)[1] = sel == 1 ? 255 : 0;
				_R(i, j)[2] = sel == 2 ? 255 : 0;
			}
		res = _R;
		break;
	}
	return res;
}


//================================= LAB 1=======================================================


void color2Grey(cv::Mat& I, const unsigned& i, const unsigned& j) {
	CV_Assert(I.depth() != sizeof(uchar));
	cv::Mat  res(I.rows, I.cols, CV_8UC3);
	switch (I.channels()) {
	case 3:
		cv::Mat_<cv::Vec3b> _I = I;
		int B = (_I(i, j)[0] + _I(i, j)[1] + _I(i, j)[2]) / 3;
		_I(i, j)[0] = B;
		_I(i, j)[1] = _I(i, j)[0];
		_I(i, j)[2] = _I(i, j)[0];
		I = _I;
		break;
	}
}

void color2BW(cv::Mat_<cv::Vec3b> &_I, const unsigned& i, const unsigned& j) {
	int B = (_I(i, j)[0] + _I(i, j)[1] + _I(i, j)[2]) / 3;
	if (B < 100)
		_I(i, j)[0] = _I(i, j)[1] = _I(i, j)[2] = 0;
	else
		_I(i, j)[0] = _I(i, j)[1] = _I(i, j)[2] = 255;
}


void brighten(cv::Mat& I, const unsigned& i, const unsigned& j) {
	CV_Assert(I.depth() != sizeof(uchar));
	cv::Mat  res(I.rows, I.cols, CV_8UC3);
	switch (I.channels()) {
	case 3:
		cv::Mat_<cv::Vec3b> _I = I;
		for (int c = 0; c < 3; c++) {
			int color = _I(i, j)[c] + 100;
			if (color > 255)
				_I(i, j)[c] = 255;
			else
				_I(i, j)[c] = color;
		}
		I = _I;
		break;
	}
}

void contrast(cv::Mat& I, const unsigned& i, const unsigned& j) {
	CV_Assert(I.depth() != sizeof(uchar));
	cv::Mat  res(I.rows, I.cols, CV_8UC3);
	switch (I.channels()) {
	case 3:
		cv::Mat_<cv::Vec3b> _I = I;
		for (int c = 0; c < 3; c++) {
			int color = _I(i, j)[c] * 1.5;
			if (color > 255)
				_I(i, j)[c] = 255;
			else
				_I(i, j)[c] = color;
		}
		I = _I;
		break;
	}
}



void pobrLab1Filter(cv::Mat& I) {
	for (int i = 0; i < I.rows; ++i)
		for (int j = 0; j < I.cols; ++j) {
			if (j<i && i > I.cols - j)
				color2Grey(I, j, i);
			else if (j<i && i <= I.cols - j)
				brighten(I, j, i);
			else if (j >= i && i <= I.cols - j)
				contrast(I, j, i);
		};
}


void histogram(cv::Mat& I) {
	CV_Assert(I.depth() != sizeof(uchar));
	cv::Mat  res(I.rows, I.cols, CV_8UC3);
	switch (I.channels()) {
	case 3:
		cv::Mat_<cv::Vec3b> _I = I;
		int B;
		const int l = 256;
		int H[l];
		for (int i = 0; i < l; ++i) {
			H[i] = 0;
		}
		int Sum = 0;
		for (int i = 0; i < I.rows; ++i)
			for (int j = 0; j < I.cols; ++j) {
				B = (_I(i, j)[0] + _I(i, j)[1] + _I(i, j)[2]) / 3;
				B /= 32;
				if (B < l) ++(H[B]);
			}
		for (int i = 0; i < 9; ++i) {
			Sum += H[i];
			std::cout << i << ": " << H[i] << std::endl;
		}
		std::cout << "Suma: " << Sum << std::endl;
	};
}

// ============================== LAB 2 ========================================

void normalize_color(float color[3]) {
	for (int c = 0; c < 3; ++c) {
		if (color[c] > 255)
			color[c] = 255;
		else if (color[c] < 0)
			color[c] = 0;
	}
}


int brightness(cv::Mat& I, const unsigned& i, const unsigned& j) {
	CV_Assert(I.depth() != sizeof(uchar));
	cv::Mat  res(I.rows, I.cols, CV_8UC3);
	switch (I.channels()) {
	case 3:
		cv::Mat_<cv::Vec3b> _I = I;
		int B = (_I(i, j)[0] + _I(i, j)[1] + _I(i, j)[2]) / 3;
		return B;
		break;
	}
}

void filter_3x3(cv::Mat_<cv::Vec3b>& I, cv::Mat_<cv::Vec3b>& _I, const unsigned& i, const unsigned& j, const float f[3][3]) {
	CV_Assert(I.depth() != sizeof(uchar));
	cv::Mat  res(I.rows, I.cols, CV_8UC3);
	switch (I.channels()) {
	case 3:
		float f_sum = 0;
		float sum[3];

		for (int x = 0; x < 3; ++x)
			for (int y = 0; y < 3; ++y)
				f_sum += f[x][y];
		for (int c = 0; c < 3; ++c) {
			sum[c] = 0;
			for (int x = 0; x < 3; ++x) {
				for (int y = 0; y < 3; ++y) {
					sum[c] += I(x + i - 1, y + j - 1)[c] * f[x][y];
				}
			}
			if (f_sum != 0)
				sum[c] = sum[c] / f_sum;
		}
		normalize_color(sum);
		_I(i, j)[0] = sum[0];
		_I(i, j)[1] = sum[1];
		_I(i, j)[2] = sum[2];
		break;
	}
}




void pobrLab2Filter(cv::Mat& I, const float f[3][3], const float fh[3][3], int bright) {
	cv::Mat_<cv::Vec3b> J = I;
	cv::Mat_<cv::Vec3b> _I(I.rows, I.cols);

	for (int i = 1; i < _I.rows - 1; ++i)
		for (int j = 1; j < _I.cols - 1; ++j) {
			if (brightness(I, i, j) >= bright)
				filter_3x3(J, _I, i, j, f);
			else
				filter_3x3(J, _I, i, j, fh);
		};
	I = _I;
}


//=============================================== LAB III ===================================================


double mom(int p, int q, cv::Mat_<cv::Vec3b>& I) { //moment pq
	CV_Assert(I.depth() != sizeof(uchar));
	cv::Mat  res(I.rows, I.cols, CV_8UC3);
	double m = 0;
	switch (I.channels()) {
	case 3:
		for (int i = 0; i < I.rows; ++i)
			for (int j = 0; j < I.cols; ++j) {
				if (I(i, j)[0] == 0) //naley do body
					m += pow(i, p) * pow(j, q);
			};
		break;
	};
	return m;
}

double Mom(int p, int q, double i_cen, double j_cen, cv::Mat_<cv::Vec3b>& I) { //moment pq
	CV_Assert(I.depth() != sizeof(uchar));
	cv::Mat  res(I.rows, I.cols, CV_8UC3);
	double m = 0;
	switch (I.channels()) {
	case 3:
		for (int i = 0; i < I.rows; ++i)
			for (int j = 0; j < I.cols; ++j) {
				if (I(i, j)[0] == 0) //naley do body
					m += pow(i - i_cen, p) * pow(j - j_cen, q);
			};
		break;
	};
	return m;
}


enum pix_type { EDGE, BODY, BACK };
typedef std::pair <int, int> pix;
pix_type filter_edge(cv::Mat_<cv::Vec3b>& I, const unsigned& i, const unsigned& j) {
	CV_Assert(I.depth() != sizeof(uchar));
	cv::Mat  res(I.rows, I.cols, CV_8UC3);
	switch (I.channels()) {
	case 3:
		for (int x = i - 1; x <= i + 1; ++x)
			for (int y = j - 1; y <= j + 1; ++y) {
				if (x != i && y != j && I(i, j)[0] == 0) {
					if (I(x, y)[0] != 0) {
						I(i, j)[2] = 255;
						return EDGE;

					}
					else if (x == i + 1 && y == j + 1) {
						I(i, j)[1] = 127;
						return BODY;
					}
				}
			}
		break;
	}
	return BACK;
}



void pobrLab3(cv::Mat& I) {
	cv::Mat_<cv::Vec3b> J = I;
	int body_pixs = 0, edge_pixs = 0, bck_pixs = 0;
	float W3 = 0;
	for (int i = 1; i < J.rows - 1; ++i)
		for (int j = 1; j < J.cols - 1; ++j) {
			switch (filter_edge(J, i, j)) {
			case EDGE: ++body_pixs; ++edge_pixs; break;
			case BODY: ++body_pixs; break;
			case BACK: ++bck_pixs; break;
			};
		};
	double L = edge_pixs;
	double S = body_pixs;
	W3 = L / (2 * sqrt(3.14 * S)) - 1;
	double i_cent, j_cent;
	double m00 = mom(0, 0, J);
	i_cent = mom(1, 0, J) / m00;
	j_cent = mom(0, 1, J) / m00;
	double M30 = Mom(3, 0, i_cent, j_cent, J);
	double M12 = Mom(1, 2, i_cent, j_cent, J);
	double M21 = Mom(2, 1, i_cent, j_cent, J);
	double M03 = Mom(0, 3, i_cent, j_cent, J);

	double M20 = Mom(2, 0, i_cent, j_cent, J);
	double M02 = Mom(0, 2, i_cent, j_cent, J);
	double M11 = Mom(1, 1, i_cent, j_cent, J);

	double M7 = (M20 * M02 - pow(M11, 2)) / pow(m00, 4);
	double M3 = (pow(M30 - 3 * M12, 2) + pow(3 * M21 - M03, 2)) / pow(m00, 5);


	std::cout << "L:" << L << " S:" << S << " W3:" << W3 << " M3:" << M3 << " M7:" << M7 << std::endl;
	//std::cout << "M30:" << M30 << " M12:" << M12 << " M03:" << M03 << " M21:" << M21 << std::endl;
	//std::cout << "i_cent: " << i_cent << " j_cent: " << j_cent << std::endl;
	I = J;
}

struct mb_object {
	std::list<pix> pixels;
	std::list<pix> pixels_on_box;
	cv::Point cent, cent_m, p1, p2;
	double m00 = 0, m01 = 0, m10 = 0;
};

void color2BW(cv::Mat& I) {
	cv::Mat_<cv::Vec3b> J = I;
	int body_pixs = 0, edge_pixs = 0, bck_pixs = 0;
	float W3 = 0;
	for (int i = 1; i < J.rows - 1; ++i)
		for (int j = 1; j < J.cols - 1; ++j) {
			color2BW(J, i, j);
		};
	I = J;
}

void cut_ages(cv::Mat& I) {
	cv::Mat_<cv::Vec3b> J = I;
	cv::Mat_<cv::Vec3b> K = I.clone();
	int body_pixs = 0, edge_pixs = 0, bck_pixs = 0;
	float W3 = 0;
	for (int i = 0; i < J.rows; ++i)
		for (int j = 0; j < J.cols; ++j) {
			switch (filter_edge(J, i, j)) {
			case BODY: K(i, j)[0] = K(i, j)[1] = K(i, j)[2] = 0; break;
			case EDGE: K(i, j)[0] = K(i, j)[1] = K(i, j)[2] = 255; break;
			};
		};
	I = K;
}

double kat(cv::Point a, cv::Point b) {

	const double PI = 3.14159265;
	//std::cout << int(J(obj->cent.y, obj->cent.x)[0]) << "," << int(J(obj->cent.y, obj->cent.x)[1]) << "," << int(J(obj->cent.y, obj->cent.x)[2]);
	double x_l = a.x - b.x;
	//if (x_l < 0) x_l *= -1;
	double y_l = a.y - b.y;
	//if (y_l < 0) y_l *= -1;
	//std::cout << " x:" << x_l << " y:" << y_l << std::endl;
	double kat = 0;
	if (x_l != 0) {
		kat = atan(-y_l / x_l)* 180.0 / PI;
		if (kat < 0) kat += 180;
	}
	else {
		kat = 90;
	}
	return kat;
};
void box(cv::Mat& I, int zad = 1) {
	std::map<int, mb_object> objects;
	std::map<int, mb_object>::iterator it;
	cv::Mat_<cv::Vec3b> J = I;
	CV_Assert(I.depth() != sizeof(uchar));
	cv::Mat  res(J.rows, J.cols, CV_8UC3);

	switch (J.channels()) {
	case 3:
		for (int i = 0; i < J.rows; ++i)
			for (int j = 0; j < J.cols; ++j) {
				if (J(i, j)[1] == 255)
					continue;
				it = objects.find(J(i, j)[1]);
				/*if (it == objects.end()) {
				mb_object *object = new mb_object;
				objects[J(i, j)[1]] = *object;
				}*/
				pix p = std::make_pair(i, j);
				(objects[J(i, j)[1]]).pixels.push_front(p);
			};
		break;
	};
	//std::cout << "objects.size() is " << objects.size() << '\n';
	int index = 0;
	for (auto it = objects.begin(); it != objects.end() && index < 5; ++it, ++index) {
		mb_object *obj = &(it->second);
		obj->m00 = 0;
		obj->m10 = 0;
		obj->m01 = 0;
		int maxx = 0, minx = 0;
		int maxy = 0, miny = 0;
		for (auto list_it = (it->second).pixels.begin(); list_it != (it->second).pixels.end(); ++list_it) {
			int x = list_it->first;
			int y = list_it->second;
			if (list_it == (it->second).pixels.begin()) {
				maxx = minx = x;
				maxy = miny = y;
			};
			//J(list_it->first, list_it->second)[1] = index * 20;
			if (maxx < x) maxx = x;
			if (minx > x) minx = x;
			if (maxy < y) maxy = y;
			if (miny > y) miny = y;
			obj->m00 += pow(x, 0) * pow(y, 0);
			obj->m10 += pow(x, 1) * pow(y, 0);
			obj->m01 += pow(x, 0) * pow(y, 1);
		}

		cv::Point p1, p2;
		p1.x = maxy;
		p1.y = maxx;
		p2.x = miny;
		p2.y = minx;

		obj->cent.x = (p1.x + p2.x) / 2;
		obj->cent.y = (p1.y + p2.y) / 2;
		obj->p1 = p1;
		obj->p2 = p2;
		obj->cent_m.y = obj->m10 / obj->m00;
		obj->cent_m.x = obj->m01 / obj->m00;

		std::cout << int(J(obj->cent.y, obj->cent.x)[0]) << "," << int(J(obj->cent.y, obj->cent.x)[1]) << "," << int(J(obj->cent.y, obj->cent.x)[2]);
		if (zad == 1) {
			std::cout << " kat: " << kat(obj->cent_m, obj->cent) << std::endl;
			cv::circle(J, obj->cent, 4, 0, -1);
		}
		else {
			for (auto list_it = (it->second).pixels.begin(); list_it != (it->second).pixels.end(); ++list_it) {
				int x = list_it->first;
				int y = list_it->second;
				if (x == obj->p1.y || x == obj->p2.y || y == obj->p1.x || y == obj->p2.x) {
					obj->pixels_on_box.push_front(*list_it);
				};
			};
			int o_x = 0, o_y = 0;
			int o_i = 0;
			for (auto p_it = obj->pixels_on_box.begin(); p_it != obj->pixels_on_box.end(); ++p_it) {
				cv::Point p(p_it->second, p_it->first);
				double res = cv::norm(p - obj->cent_m);
				if (res > 63 && res < 66) {

					o_i++;
					o_x += p.x;
					o_y += p.y;
				}
			}
			if (o_i > 0) {
				cv::Point ostrze(o_x / o_i, o_y / o_i);
				cv::circle(J, ostrze, 4, 0, -1);
				std::cout << " kat: " << -kat(ostrze, obj->cent_m) + 180;
				std::cout << std::endl;
			}
		}
		cv::circle(J, obj->cent_m, 4, 0, -1);
		cv::rectangle(J, p2, p1, 0);
	}
	I = J;
}

int main(int, char *[]) {
	std::cout << "Start ..." << std::endl;

	//cv::Mat image = cv::imread("Lena.png");
	//cv::Mat image2 = image(cv::Rect(100,100,100,100));
	//perform(image2);

	//cv::Mat max = selectMax(image);
	//cv::imshow("Lena",image);
	//cv::imshow("Max",max);

	//std::cout << image2.isContinuous() << max.isContinuous() << std::endl;
	//cv::imwrite("Max.png",max);

	//cv::waitKey(-1);



	//cv::Mat max = color2Grey(image);

	//================ LAB I ===============================
	/*
	cv::Mat image = cv::imread("Lena.png");
	histogram(image);
	pobrLab1Filter(image);
	pobrLab1Filter(image);
	histogram(image);
	cv::imshow("Lena",image);
	//cv::imshow("Max",max);
	*/

	//================ LAB II =================================
	/*

	cv::Mat image = cv::imread("Lena.png");
	cv::Mat image_f = cv::imread("Lena.png");
	cv::Mat image_h = cv::imread("Lena.png");
	cv::Mat image_l = cv::imread("Lena.png");


	float f[3][3];
	float fh[3][3];
	f[0][0] = 1; f[1][0] = 1; f[2][0] = 1;
	f[0][1] = 1; f[1][1] = 1; f[2][1] = 1;
	f[0][2] = 1; f[1][2] = 1; f[2][2] = 1;

	fh[0][0] = -1; fh[1][0] = -1; fh[2][0] = -1;
	fh[0][1] = -1; fh[1][1] = 8; fh[2][1] = -1;
	fh[0][2] = -1; fh[1][2] = -1; fh[2][2] = -1;


	pobrLab2Filter(image_f, f, fh, 125);
	pobrLab2Filter(image_l, f, fh, 255);
	pobrLab2Filter(image_h, f, fh, 0);
	cv::imshow("Lena F", image_f);
	cv::imshow("Lena H", image_h);
	cv::imshow("Lena L", image_l);
	cv::imshow("Lena", image);

	cv::waitKey(-1);
	*/

	//=============== LAB III ======================================
	// policzyc: S,L,W3,M3,M7
	
	bool show_imgs = true, _zad1 = true, _zad2 = true;
	if (_zad1) {

		cv::Mat prost = cv::imread("prost.dib");
		cv::Mat kolo = cv::imread("kolo.dib");
		cv::Mat troj = cv::imread("troj.dib");
		cv::Mat elipsa = cv::imread("elipsa.dib");
		cv::Mat elipsa1 = cv::imread("elipsa1.dib");

		std::cout << "Prost" << std::endl;
		pobrLab3(prost);
		std::cout << std::endl << "kolo" << std::endl;
		pobrLab3(kolo);
		std::cout << std::endl << "troj" << std::endl;
		pobrLab3(troj);
		std::cout << std::endl << "elipsa" << std::endl;
		pobrLab3(elipsa);
		std::cout << std::endl << "elipsa1" << std::endl;
		pobrLab3(elipsa1);

		if (show_imgs) {
			cv::imshow("prost", prost);
			cv::imshow("kolo", kolo);
			cv::imshow("trojkat", troj);
			cv::imshow("elipsa", elipsa);
			cv::imshow("elipsa 1", elipsa1);
		}
	};

	if (_zad2) {
		cv::Mat strzalki_1 = cv::imread("strzalki_1.dib");
		cv::Mat strzalki_2 = cv::imread("strzalki_2.dib");
		std::cout << std::endl << "strzalki_1" << std::endl << std::endl;
		box(strzalki_1);
		std::cout << std::endl << "strzalki_2" << std::endl << std::endl;
		//color2BW(strzalki_2);

		//cut_ages(strzalki_2);
		box(strzalki_2, 2);
		if (show_imgs) {
			cv::imshow("strzalki_1", strzalki_1);
			cv::imshow("strzalki_2", strzalki_2);
		}
	}
	
	cv::waitKey(-1);

	return 0;
}
