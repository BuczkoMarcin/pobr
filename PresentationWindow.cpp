#include "PresentationWindow.h"



PresentationWindow::PresentationWindow(FiguryFinder & zdjecie1, FiguryFinder & zdjecie2, FiguryFinder & zdjecie3, const std::string& name):
	MyWindow(zdjecie1, name, trackbar_vect_t({ {0,1,"szachownica"} })),
		scena{{&zdjecie1, Szachownica(zdjecie1), Klocek(zielonyKlocekProperties), Klocek(czerwonyKlocekProperties), Walec(walecProperties), cv::Mat(), false}, 
		      {&zdjecie2, Szachownica(zdjecie2), Klocek(zielonyKlocekProperties), Klocek(czerwonyKlocekProperties), Walec(walecProperties), cv::Mat(), false },
			  {&zdjecie3, Szachownica(zdjecie3), Klocek(zielonyKlocekProperties), Klocek(czerwonyKlocekProperties), Walec(walecProperties), cv::Mat(), false }}
{
	scaleFigury(2.5);
	cv::Size size = _img->getImage().size();
	double height = 400;
	cv::resizeWindow(_name, (size.width * double(height / (2 * size.height)))*3.0, height);

	_masks_s.push_back(Mask::RED);
	_masks_s.push_back(Mask::GREEN);
	_masks_s.push_back(Mask::CYLINDER);
	_masks_s.push_back(Mask::NULL_MASK);

	for (size_t i = 0; i < 3; ++i) {
		_img = scena[i]._zdjecie;
		register_window();
		screen_1line[i] = scena[i]._zdjecie->getImage();
		screen_2line[i] = scena[i]._zdjecie->getImage();
		//scena[i]._zdjecie->doMask(_masks_s);
	}

	process();
	redraw_scena(0);
	redraw_scena(1);
	redraw_scena(2);
	refresh_window();
}

PresentationWindow::~PresentationWindow()
{

	for (size_t i = 0; i < 3; ++i) {
		_img = scena[i]._zdjecie;
		unregister_window();
	}
}

void PresentationWindow::do_callback()
{
	redraw_scena(0);
	redraw_scena(1);
	redraw_scena(2);
	refresh_window();
}

void PresentationWindow::onWindowsChanged()
{
}

// ====================== SCALE DEFINITONS ==============================

void  PresentationWindow::scaleFigury(const double& scale) {
	for (size_t i = 0; i < 3; ++i) {
		scena[i].s.scaleDefinition(scale);
		scena[i].zielony.scaleDefinition(scale);
		scena[i].czerwony.scaleDefinition(scale);
		scena[i].walec.scaleDefinition(scale);
	}
}


// ======================= F I N D ======================================
void  PresentationWindow::findFigury() {
	for (size_t i = 0; i < 3; ++i)
		findFigurySceny(scena[i]);
}

void  PresentationWindow::findFigurySceny(Scena& s) {
		//findSzachownica(s);
		findKlocek(s, s.czerwony, 0);
		findKlocek(s, s.zielony, 1);
		findWalec(s);
}

void PresentationWindow::findSzachownica(Scena& s) {
	lines_t h_lines;
	s._zdjecie->reset();
	s._zdjecie->doFilter();
	s._zdjecie->doCanny();
	s._zdjecie->doHough(h_lines);
	s.s.setChessboardLines(h_lines);
	s.s.find_Chessboard();
	s._is_matrix = false;
	if (s.s.getMatrix3DTo2D(s.matrix3Dto2D))
		s._is_matrix = true;
}

void PresentationWindow::findKlocek(Scena& s, Klocek& k, const int& i) {
	if (!s._is_matrix)
		return;
	k.setMatrix3DTo2D(s.matrix3Dto2D);
	//s._zdjecie->doFilter();
	//s._zdjecie->resetMasked_F();
	//s._zdjecie->doMask(_masks_s);
	//s._zdjecie->findContours();
	//preProcess(s._zdjecie);
	contours_t contoures = s._zdjecie->getConture_F(i).contours;
	if (contoures.size() > 0) {
		k.setExpirienced3D(contour2i_to_2f(s._zdjecie->getConture_F(i).contours[0]));
	};
	k.findPrzedmiot();
}

void PresentationWindow::findWalec(Scena& s) {
	if (!s._is_matrix)
		return;
	s.walec.setMatrix3DTo2D(s.matrix3Dto2D);

	//preProcess(s._zdjecie);
	//s._zdjecie->resetMasked_F();
	//s._zdjecie->doMask();
	//s._zdjecie->findContours();
	contours_t contoures = s._zdjecie->getConture_F(2).contours;
	if (contoures.size() > 0) {
		s.walec.setExpirienced3D(contour2i_to_2f(s._zdjecie->getConture_F(2).contours[0]));
	};
	s.walec.findPrzedmiot();
}

// ===================== P R O C E S ======================================

void PresentationWindow::process() {
	for (size_t i = 0; i < 3; ++i) {
		findSzachownica(scena[i]);
		preProcess(scena[i]._zdjecie);
		findFigurySceny(scena[i]);
		process(scena[i]._zdjecie, i);
	};
}


// wykonuje filtrowanie, generowanie masek i znajdowanie kontur�w obiekt�w.
void PresentationWindow::preProcess(FiguryFinder* f) {
	f->reset();
	f->doFilter(true);
	f->doMask(_masks_s);
	f->resetMasked_F();
	f->findContours();
}

// znajduje kontury na podstawie masek i generuje obraz z maskami na screenie i
void PresentationWindow::process(FiguryFinder* f, const int& i) {

	//FiguryFinder f(*zdjecie);
	//preProcess(f);
	cv::Mat masked = f->getImage();
	//cv::Mat masked = cv::Mat::zeros(f->getImage().size(), f->getImage().type());
	masked = f->_doFiguraImg(0, masked);
	masked = f->_doFiguraImg(1, masked);
	masked = f->_doFiguraImg(2, masked);
	
	if (i < 3) {
		screen_2line[i] = masked.clone();
	};
}

// ==================== D R A W ===========================================

void PresentationWindow::redraw_scena(const int& i) {


	cv::Mat _img_szachy = scena[i]._zdjecie->getResult();
	
	scena[i].s.draw3D(_img_szachy);
	scena[i].zielony.draw3D(_img_szachy);
	scena[i].czerwony.draw3D(_img_szachy);
	scena[i].walec.draw3D(_img_szachy);


	if (MyWindow::_trackbars[0].value == 1) {
		scena[i].s.drawFound(_img_szachy);
		scena[i].zielony.drawFound(_img_szachy);
		scena[i].czerwony.drawFound(_img_szachy);
		scena[i].walec.drawFound(_img_szachy);
	}

	screen_1line[i] = _img_szachy;

}

void PresentationWindow::refresh_window() 
{
	try {
		cv::Mat line1, line2;
		cv::hconcat(screen_1line[2], screen_1line[1], line1);
		cv::hconcat(screen_1line[0], line1, line1);
		
		cv::hconcat(screen_2line[2], screen_2line[1], line2);
		cv::hconcat(screen_2line[0], line2, line2);

		cv::vconcat(line1, line2, line1);
		
		cv::imshow(_name, line1);
	}
	catch (std::exception e) {
		std::cout << e.what() << std::endl;
	};
}
