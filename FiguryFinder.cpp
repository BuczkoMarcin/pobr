#include "FiguryFinder.h"


void form_CvSeq_to_vector(CvSeq* src, contours_t& dst) {
	dst.clear();
	int j = 0;
	CvSeq* start = src;
	while (src)
	{
		dst.push_back(cv::vector< cv::Point >());
		for (int i = 0; i < src->total; ++i) {
			dst[j].push_back(cv::Point(
				((CvPoint*)cvGetSeqElem(src, i))->x,
				((CvPoint*)cvGetSeqElem(src, i))->y));
		}
		j++;
		src = src->h_next;
	};
	src = start;
}

void FiguryFinder::init()
{
}

void FiguryFinder::resetMasked_F()
{
	_masked_F.clear();
	for (auto mask : _masked) {
		_masked_F.push_back(mask);
		_contours_F.push_back(contours_h_t());
	};
}

bool FiguryFinder::setMasked_F(const int & i, const cv::Mat & maskedF)
{
	if(!is_index_masked_F_ok(i))
		return false;
	_masked_F[i] = maskedF;
	return true;
}

bool FiguryFinder::setContour_F(const int & i, const contours_h_t & contours)
{
	if (!is_index_contours_F_ok(i))
		return false;
	_contours_F[i] = contours;
	return true;
}

void FiguryFinder::__findContours(const cv::Mat& mask, contours_h_t& dst_)
{

	IplImage img = mask;
	CvSeq* contours;  //hold the pointer to a contour in the memory block
	CvMemStorage *storage = cvCreateMemStorage(0); //storage area for all contours

												   //finding all contours in the image
	cvFindContours(&img, storage, &contours, sizeof(CvContour), CV_RETR_LIST, CV_CHAIN_APPROX_TC89_KCOS, cvPoint(0, 0));
	contours_h_t tmp;
	form_CvSeq_to_vector(contours, tmp.contours);
	cvReleaseMemStorage(&storage);

	int max_i = 0;
	double max_area = 0;
	double area;
	for (size_t i = 0; i < tmp.contours.size(); ++i) {
		area = cv::contourArea(tmp.contours[i]);
		if (area > max_area) {
			max_area = area;
			max_i = i;
		}
	}

	dst_.contours.clear();
	if(max_area>1000)
		dst_.contours.push_back(tmp.contours[max_i]);

	//cout << "coutures" << endl;
	//for (auto& contour : dst_.contours) {
	//	for (auto& point : contour)
	//		cout << point << " ";
	//	cout << endl;
	//};
	//cout << endl;


	for (size_t i = 0; i < dst_.contours.size(); i++)
	{
		//cv::approxPolyDP(cv::Mat(dst_.contours[i]), dst_.contours[i], 20, true);
		cv::convexHull(dst_.contours[i], dst_.contours[i]);
	}
	



}

const contours_h_t FiguryFinder::_findContours(const int & i)
{
	if (!is_index_contours_F_ok(i) || !is_index_masks_ok(i))
		return contours_h_t();
	cv::Mat mask;
	_masks[i].getMask(mask);
	__findContours(mask, _contours_F[i]);
	return _contours_F[i];
}

const cv::Mat FiguryFinder::_doFiguraImg(const int & i) const
{

	//if (!is_index_masks_ok(i))
	//	return cv::Mat(_result.size(), _result.type());
	//
	//cv::Mat mask;
	//_masks[i].getMask(mask);
	//return mask;

	if(!is_index_masked_F_ok(1) || !is_index_contours_F_ok(i))
		return cv::Mat(_result.size(), _result.type());
	return _doFiguraImg(i, _masked_F[i].clone());
}

const cv::Mat FiguryFinder::_doFiguraImg(const int & i, const cv::Mat & back) const
{
	if (!is_index_masked_F_ok(1) || !is_index_contours_F_ok(i))
		return cv::Mat(_result.size(), _result.type());

	cv::RNG rng(12345);
	contours_t contours = _contours_F[i].contours;
	hierarchy_t hierarchy = _contours_F[i].hierarchy;

	/// Draw contours
	// cv::Mat drawing = _masked_F[i].clone();
	cv::Mat drawing = back.clone();
	for (size_t j = 0; j< contours.size(); j++)
	{
		cv::Scalar color = cv::Scalar(rng.uniform(0, 255), rng.uniform(0, 255), rng.uniform(0, 255));
		drawContours(drawing, contours, j, color, 2, 8, cv::noArray(), 0, cv::Point());
	}

	//vector<cv::RotatedRect> minRect(contours.size());
	//vector<cv::RotatedRect> minEllipse(contours.size());
	//
	//for (int i = 0; i < contours.size(); i++)
	//{
	//	minRect[i] = cv::minAreaRect(cv::Mat(contours[i]));
	//	if (contours[i].size() > 5)
	//	{
	//		minEllipse[i] = cv::fitEllipse(cv::Mat(contours[i]));
	//	}
	//}

	for (size_t i = 0; i< contours.size(); i++)
	{
		cv::Scalar color = cv::Scalar(rng.uniform(0, 255), rng.uniform(0, 255), rng.uniform(0, 255));
		// contour
		drawContours(drawing, contours, i, color, 1, 8, vector<cv::Vec4i>(), 0, cv::Point());
		// ellipse
		//ellipse(drawing, minEllipse[i], color, 2, 8);
		// rotated rectangle
		//cv::Point2f rect_points[4]; minRect[i].points(rect_points);
		//for (int j = 0; j < 4; j++)
		//	line(drawing, rect_points[j], rect_points[(j + 1) % 4], color, 1, 8);
	}




	return drawing;
}

const contours_F_t FiguryFinder::findContours()
{
	_contours_F.clear();
	cv::Mat mask;
	contours_h_t contours;
	for (size_t i = 0; i < _masks.size(); ++i) {
		_masks[i].getMask(mask);
		_contours_F.push_back(contours_h_t());
		__findContours(mask, _contours_F[i]);

	}
	return _contours_F;
}

const cv::Mat FiguryFinder::getMasked_F(const int & i) const
{
	if (is_index_masked_F_ok(i))
		return _masked_F[i];
	return cv::Mat();
}

const contours_h_t FiguryFinder::getConture_F(const int & i) const
{
	if (is_index_contours_F_ok(i))
		return _contours_F[i];
	return contours_h_t();
}

