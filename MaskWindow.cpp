#include "MaskWindow.h"




void MaskWindow::onWindowsChanged()
{
	regenerateMasks(true);
	refreshScreeen(true);
	show_screen();

}

void MaskWindow::set_MaskSettings_From_Trackbars()
{
	HSV_setings tR_HSV = {
		_trackbars[0].value,
		_trackbars[1].value,
		_trackbars[2].value,
		_trackbars[3].value,
		_trackbars[4].value,
		_trackbars[5].value
	};
	if (!setMask_s(_trackbars[6].value-1, tR_HSV)){
		setMask_s(0,Mask::RED);
		setMask_s(1,Mask::GREEN);
		setMask_s(2,Mask::CYLINDER);
		setMask_s(3,Mask::NULL_MASK);
	}
}

void MaskWindow::set_Trackbars_From_MaskSettings()
{
	HSV_setings mask_HSV;
	if (!getMask_s(_trackbars[6].value - 1, mask_HSV)) return;

	_trackbars[0].value = mask_HSV.min_h;
	_trackbars[1].value = mask_HSV.max_h;
	_trackbars[2].value = mask_HSV.min_s;
	_trackbars[3].value = mask_HSV.max_v;
	_trackbars[4].value = mask_HSV.min_v;
	_trackbars[5].value = mask_HSV.max_v;

	cv::setTrackbarPos(_trackbars[0].name, _name, _trackbars[0].value);
	cv::setTrackbarPos(_trackbars[1].name, _name, _trackbars[1].value);
	cv::setTrackbarPos(_trackbars[2].name, _name, _trackbars[2].value);
	cv::setTrackbarPos(_trackbars[3].name, _name, _trackbars[3].value);
	cv::setTrackbarPos(_trackbars[4].name, _name, _trackbars[4].value);
	cv::setTrackbarPos(_trackbars[5].name, _name, _trackbars[5].value);

}


void MaskWindow::_refreshScreeen(int screen_nr)
{
	cv::Mat masked;
	try {
		int i = screen_nr - 1;
		if(!_img_r->getMasked(i, masked))
			return;
	}
	catch (std::exception e) {
		cout << e.what() << endl;
	}

	switch (screen_nr) {
	case 1: setScreen_1(masked); break;
	case 2: setScreen_2(masked); break;
	case 3: setScreen_3(masked); break;
	case 4: setScreen_4(masked); break;
	default: return;
	}
}

void MaskWindow::refreshScreeen(const bool& all)
{
	_refreshScreeen(1);
	_refreshScreeen(2);
	_refreshScreeen(3);
	if(all) _refreshScreeen(4);
}

void MaskWindow::_regenerateMasks(int screen_nr)
{
	try {
		int i = screen_nr - 1;
		_img_r->_doMask(i, _masks_s[i]);
	}
	catch (std::exception e) {
		cout << e.what() << endl;
	}
}

void MaskWindow::regenerateMasks(const bool& all)
{
	_regenerateMasks(1);
	_regenerateMasks(2);
	_regenerateMasks(3);
	if (all) _regenerateMasks(4);
}

bool MaskWindow::setMask_s(const int & i, const HSV_setings& mask_s)
{
	if (i < 0 || i >= _masks_s.size())
		return false;
	_masks_s[i] = mask_s;
	return true;
}

bool MaskWindow::getMask_s(const int & i, HSV_setings& mask_s)
{
	if (i < 0 || i >= _masks_s.size())
		return false;
	mask_s = _masks_s[i];
	return true;
}

void MaskWindow::do_Quadruple_callback()
{
	if (_last_mask_index != _trackbars[6].value && _trackbars[6].value == 0) {
		set_MaskSettings_From_Trackbars();
		_regenerateMasks(1);
		_regenerateMasks(2);
		_regenerateMasks(3);
		_refreshScreeen(1);
		_refreshScreeen(2);
		_refreshScreeen(3);
	}
	else if (_last_mask_index != _trackbars[6].value && _trackbars[6].value != 0) {
		set_Trackbars_From_MaskSettings();
	}
	else if(_last_mask_index == _trackbars[6].value && _trackbars[6].value != 0) {
		set_MaskSettings_From_Trackbars();
		_regenerateMasks(_trackbars[6].value);
		_refreshScreeen(_trackbars[6].value);
	}
	else {

	}
	_last_mask_index = _trackbars[6].value;
}

MaskWindow::MaskWindow(MaskReactor & img) :
	QuadrupleWindow(img, "Masks", trackbar_vect_t({
	 {0,255,"h: min"},
	 {9,255,"h: max"},
	 {172,255,"s: min"},
	 {255,255,"s: max"},
	 {0,255,"v: min"},
	 {255,255,"v: max"},
	 {0,4, "screen"}
})), 

	_last_mask_index(-1),
	_img_r(&img)
{
	_masks_s.push_back(Mask::RED);
	_masks_s.push_back(Mask::GREEN);
	_masks_s.push_back(Mask::CYLINDER);
	_masks_s.push_back(Mask::NULL_MASK);
	_img_r->doMask(_masks_s);
	
	//_masks.push_back(Mask(getImage(), Mask::RED)); 
	//_masks.push_back(Mask(getImage(), Mask::GREEN));
	//_masks.push_back(Mask(getImage(), Mask::CYLINDER));
	//_masks.push_back(Mask(getImage(), Mask::NULL_MASK));

	refreshScreeen(true);
	show_screen();
}
