#include "HoughWindow.h"



HoughWindow::HoughWindow(Zdjecie & img) :
	MyWindow(img, "Hough", trackbar_vect_t({
	{ 4,  100,   "r0" },
	{ 180,  360, "theta" },
	{ 150,  360, "th" },
	{ 50,  400,  "minLength" },
	{ 10,  800,  "maxGap" },
	{ 1,  10,    "x_fact" },
	{ 1,  10,    "y_fact" }
}))
{
	do_callback();
}

void HoughWindow::do_callback()
{
	try {
		_img->setHough({ double(_trackbars[0].value),
						 double(_trackbars[1].value),
								_trackbars[2].value,
						 double(_trackbars[3].value),
						 double(_trackbars[4].value),
								_trackbars[5].value,
								_trackbars[6].value });
		_img->doHough();
		_img->houghShow(_name);
	}
	catch (std::exception e) {
		std::cout << e.what() << std::endl;
	};
}

void HoughWindow::onWindowsChanged()
{
	//cout << this << " got signal from: " << endl;
	do_callback();
};