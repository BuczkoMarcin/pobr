#pragma once
#include "Zdjecie.h"
#include "opencv2/imgproc/imgproc.hpp"
#include "pobr_types.h"

class ZdjecieZFiltrem :
	public Zdjecie
{
private:
	cv::Mat _filtered;
	Dilation_setings _d;
	Erosion_setings _e;
	cv::Mat _element_e;
	cv::Mat _element_d;
	bool _is_filtered;

	void doOpenFilter();
	void doCloseFilter();
	
	cv::Mat __doOpenFilter(const cv::Mat& src);
	cv::Mat __doCloseFilter(const cv::Mat& src);

	void init();
	void setElements();

protected:
	cv::Mat __doFilter(const cv::Mat& src);

public:

	const cv::Mat& getFilterd();

	void setFilter(const Dilation_setings& d_, const Erosion_setings& e_);
	void doOpenCloseFilter();
	void filteredShow(std::string window_name);

	ZdjecieZFiltrem(const ZdjecieZFiltrem& _zdjecieZFiltrem);
	ZdjecieZFiltrem(const cv::Mat& src) : Zdjecie(src) { init(); };
	ZdjecieZFiltrem(const std::string& file_name) : Zdjecie(file_name) { init(); };
	~ZdjecieZFiltrem();
};

