#include "Mask.h"

using namespace cv;
using namespace std;

//const HSV_setings Mask::RED =           { 0, 9,    172, 255,  165, 255 };
//const HSV_setings Mask::GREEN =         { 26, 39,  0, 255,    50, 150 };
//const HSV_setings Mask::CYLINDER =      { 11, 16,  180, 255,  12, 255 };
//const HSV_setings Mask::WHITE_SQUARES = { 9, 30,   0, 255,    0, 227 };
//const HSV_setings Mask::BLACK_SQUARES = { 9, 30,   0, 255,    0, 118 };
//const HSV_setings Mask::BACKGROUND =    { 20, 30,  0, 255,    118, 255 };
//const HSV_setings Mask::NULL_MASK =     { 0, 255,  0, 255,    0,  255 };


Mask::Mask(const cv::Mat& img, const Mask_type& type)
{
	init();
	generateMask(img,type);
}

Mask::Mask(const cv::Mat& img, const HSV_setings& m_settings) 
{
	init();
	generateMask(img,m_settings);

}

void Mask::init() {
	_mask_setings[Mask_type::RED] = RED;
	_mask_setings[Mask_type::GREEN] = GREEN;
	_mask_setings[Mask_type::CYLINDER] = CYLINDER;
	_mask_setings[Mask_type::WHITE_SQUARES] = WHITE_SQUARES;
	_mask_setings[Mask_type::BLACK_SQUARES] = BLACK_SQUARES;
	_mask_setings[Mask_type::BACKGROUND] = BACKGROUND;
}

Mask::~Mask()
{
}

void Mask::filterMask() {
	cv::Mat se1 = cv::getStructuringElement(MORPH_RECT, cv::Size(5, 5));
	cv::Mat se2 = cv::getStructuringElement(MORPH_RECT, cv::Size(2, 2));
	cv::erode(_mask, _mask, se2, cv::Point(-1, -1), 3);
	cv::dilate(_mask, _mask, se2, cv::Point(-1, -1), 3);
	//mask = cv2.dilate(mask, element, iterations = 1)
	//mask = cv2.erode(mask, element)
	//cv::morphologyEx(_mask, _mask, MORPH_CLOSE, se1);
	//cv::morphologyEx(_mask, _mask, MORPH_OPEN, se2);
}

void Mask::generateMask(const cv::Mat& img, const Mask_type& type) {
	generateMask(img, _mask_setings[type]);
}

void Mask::generateMask(const cv::Mat& img, const HSV_setings& m) {

	cvtColor(img, img, CV_BGR2HSV);
	cv::Mat_<cv::Vec3b> src = img.clone();
	cvtColor(img, img, CV_HSV2BGR);
	_mask = cv::Mat::zeros(img.size(), CV_8UC1);
	double h_, s_, v_;

	for (int x = 0; x < img.rows; ++x)
		for (int y = 0; y < img.cols; ++y) {
			h_ = src(x, y)[0];
			s_ = src(x, y)[1];
			v_ = src(x, y)[2];
			if (h_ >= m.min_h && h_ <= m.max_h &&
				s_ >= m.min_s && s_ <= m.max_s &&
				v_ >= m.min_v && v_ <= m.max_v) 
			{
				_mask.at<uchar>(x, y) = 255;
			};
		};
	
	filterMask();


}

void Mask::getMask(cv::Mat& mask_dst) const  {
	mask_dst = cv::Mat::zeros(_mask.size(), CV_8UC1);
	mask_dst = _mask.clone();
}
void Mask::showOn(cv::Mat &)
{
};

void  Mask::reGenerateMask(const cv::Mat& img, const HSV_setings& m_settings) {
	generateMask(img, m_settings);
};

void  Mask::reGenerateMask(const cv::Mat& img, const Mask_type& type) {
	generateMask(img, type);
};