#pragma once
#include "PrzedmiotSceny.h"

extern Przedmiot_properties zielonyKlocekProperties;
extern Przedmiot_properties czerwonyKlocekProperties;

class Klocek :
	public PrzedmiotSceny
	
{
	cv::Point2f vector_h;
	cv::Point2f cm;
public:
	Klocek(const contour2f_t & contour3d_exp, const cv::Mat & matrix3DTo2D);
	Klocek(const Przedmiot_properties & definition);
	Klocek();
	void setExpirienced3D(const contour2f_t & contour_exp);
	void scaleDefinition(const double & scale);
	void drawFound(cv::Mat & background) const;
	void draw2D(cv::Mat & background) const;
	void draw3D(cv::Mat & background) const;
	void drawDefined(cv::Mat & background) const;
};

