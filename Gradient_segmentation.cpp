#include "Gradient_segmentation.h"
#include <math.h> 
#include <opencv2/core/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/calib3d/calib3d.hpp>
#include <opencv2/highgui/highgui.hpp>


Gradient_segmentation::Gradient_segmentation(cv::Mat_<cv::Vec3b>& picture, const double& gradient_step):
_labels(picture), _picture(&picture), _gradient_step(gradient_step), _labels_number(0)
{
}


Gradient_segmentation::~Gradient_segmentation()
{
}

void Gradient_segmentation::operator()(){
	grad_seed.clear();
	grad_seed.push_back(t_Point(0, 0));
	Segment_labels::t_Label label = 1;
	_labels_number = 0;
	while (!grad_seed.empty()) {
		t_Point seed = grad_seed.back();
		grad_seed.pop_back();
		if (_labels(seed.first, seed.second) == Segment_labels::_null_label) {
			find_gradient(label, seed.first, seed.second);
			++label;
			++_labels_number;
		}
	};
}

void Gradient_segmentation::color_segments()
{

	uchar saturation = 255;
	uchar value = 127;
	uchar hue;
	int labels_number = getNumebOfLabels();
	if (labels_number < 2)
		return;

	cvtColor(*_picture, *_picture, CV_BGR2HSV);
	for (int i = 0; i < _picture->rows; i++)
	{
		for (int j = 0; j < _picture->cols; j++)
		{
			hue = (_labels(i, j) - 1) / (labels_number - 1);
			_picture->at<cv::Vec3b>(i, j)[0] = hue;
			_picture->at<cv::Vec3b>(i, j)[1] = saturation;
			_picture->at<cv::Vec3b>(i, j)[2] = value;
		}
	}
	cvtColor(*_picture, *_picture, CV_HSV2BGR);
}

int Gradient_segmentation::getNumebOfLabels()
{
	return _labels_number;
}


bool Gradient_segmentation::is_gradient(const cv::Vec3b& A, const cv::Vec3b B) {

	return sqrt(pow(A[0] - B[0], 2.0) + pow(A[1] - B[1], 2.0) + pow(A[2] - B[2], 2.0)) < _gradient_step;
}

void Gradient_segmentation::find_gradient(const Segment_labels::t_Label & label, const int & x, const int & y)
{

	for (int x1 = x - 1; x1 <= x + 1; x1 += 2)
		for (int y1 = y - 1; y1 <= y + 1; y1 += 2) {
			if (x == 22 && y == 948) {
				std::cout << x;
			};
			/*if (_labels(x1, y1) == Segment_labels::_null_label &&
				is_gradient((*_picture)(x,y), (*_picture)(x1, y1))) {*/
			if(_labels(x1, y1) == Segment_labels::_null_label){
				//_labels(x1, y1) = label;

				find_gradient(label, x1, y1);
			}
			else {
				//t_Point* seed_point = new t_Point(x1, y1);
				//grad_seed.push_front(*seed_point);
			}
		}
}

