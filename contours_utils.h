#pragma once
#include "opencv2\opencv.hpp"
#include "pobr_types.h"

#ifndef CONTOURS_UTILS_H_
#define CONTOURS_UTILS_H_


cv::Point2f centerOfMass(const std::vector<cv::Point2f>& contour);

cv::Point2f rotate(const double & angle, const cv::Point2f & center, const cv::Point2f & p);

void rotate(const double & angle, const cv::Point2f & center, std::vector<cv::Point2f>& contour);

void move(const cv::Point2f & vector_shift, contour2f_t& contour);

void drawContour2f(cv::Mat& img, const std::vector<cv::Point2f>& contour, const cv::Scalar& color = cv::Scalar(127, 255, 127), const int& thickness = 1);

void scaleConture(std::vector<cv::Point2f>& contour, const double& scale);

std::vector<cv::Point>  contour2f_to_2i(const std::vector<cv::Point2f>& contour);

std::vector<cv::Point2f>  contour2i_to_2f(const std::vector<cv::Point>& contour);
#endif /*CONTOURS_UTILS_H_*/ 