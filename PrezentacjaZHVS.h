#pragma once
#include "PresentationWindow.h"
#include "MyWindow.h"
class PrezentacjaZHVS :
	public PresentationWindow, public MyWindow
{

	//vector<HSV_setings> _masks_s;
	int _last_mask_index;
public:
	PrezentacjaZHVS(FiguryFinder & zdjecie1, FiguryFinder & zdjecie2, FiguryFinder & zdjecie3, const std::string& name);
	~PrezentacjaZHVS();

	// Inherited via MyWindow
	virtual void do_callback() override;
	virtual void onWindowsChanged() override;
	void process();
	void processMasks(FiguryFinder * f, const int & screen, const int & mask);
	void set_MaskSettings_From_Trackbars();
	void set_Trackbars_From_MaskSettings();
	bool setMask_s(const int & i, const HSV_setings & mask_s);
	bool getMask_s(const int & i, HSV_setings & mask_s);
};

