#include "Szachownica.h"
#include "utils.h"
#include "pobr_types.h"
#include <cmath>
#include <opencv2\opencv.hpp>


#define MB_DEBUG true
#define MB_Check( expr , msg) if( (expr) && MB_DEBUG) ; else std::cout << "[ WORRING : " << #expr  << " NOT TRUE " << msg <<"]" << std::endl;

// ============== get set  ===========================================

bool Szachownica::getMatrix3DTo2D(cv::Mat & matrix) const
{
	return PrzedmiotSceny::getMatrix3DTo2D(matrix);
}

void Szachownica::setChessboardLines(const lines_t& hought_lines)
{
	if (hought_lines.empty())
		return;
	cv::Size size;
	cv::Mat matrix;
	size = _findSizeOfScene(hought_lines);
	//size.width  += size.width;
	//size.height += size.height/10;
	__find_Chessboard_lines(hought_lines, _vlines, _hlines, matrix, size, _alpha_v, _alpha_h);
	_is_definision_from_exp = false;
	PrzedmiotSceny::reset();
}

void Szachownica::setChessboardLines(Reactor & zdjecie_) {
	lines_t hought_lines;
	Reactor zdjecie((zdjecie_.getImage()));
	//zdjecie.reset();
	zdjecie.setFilter({ 1 }, { 1 }); //filter power
	zdjecie.setCanny({ 106,198}); // th1 i th2
	zdjecie.setHough({ 4,182,150,59,68,1,1 }); // r0, theta, th, minLenght, maxGap
	zdjecie.doFilter();
	zdjecie.doCanny();
	zdjecie.doHough(hought_lines);
	setChessboardLines(hought_lines);
}
// ============== find ===========================================

bool Szachownica::find_Chessboard() {
	if (!_findContour3D_Exp()) return false;
	//if (!_findDefinition()) return false;
	if (!_findMatrix3DTo2D()) return false;
	//Przedmiot_properties definition;
	//getDefinition(definition);
	//setExpirienced2D(definition.contour2D);
	//if (!Przedmiot2D::findPrzedmiot()) return false;
	if (!PrzedmiotSceny::findPrzedmiot()) return false;
	return true;
}

bool Szachownica::find_Chessboard(const lines_t & hought_lines)
{
	setChessboardLines(hought_lines);
	if (!find_Chessboard()) return false;
	return true;
}

bool Szachownica::find_Chessboard(Reactor & zdjecie)
{
	setChessboardLines(zdjecie);
	return find_Chessboard();
}

bool Szachownica::_findContour3D_Exp()
{
	if (_hlines.empty() || _vlines.empty())
		return false;
	if (is_expirienced3D())
		return true;
	pobr::sort_horizontal_lines(_hlines);
	pobr::sort_verticla_lines(_vlines);

	contour2f_t contour3d;
	contour2f_t contour2d;
	cv::Point p1, p2, p3, p4;

	

	if (_hlines.size() < 4) {
		pobr::crosspoint(_hlines.front(), _vlines.back(), p1);
		pobr::crosspoint(_hlines.front(), _vlines.front(), p2);
		pobr::crosspoint(_hlines.back(), _vlines.front(), p3);
		pobr::crosspoint(_hlines.back(), _vlines.back(), p4);
	}
	else {
		size_t before_last = _hlines.size() - 2;
		pobr::crosspoint(_hlines[1], _vlines.back(), p1);
		pobr::crosspoint(_hlines[1], _vlines.front(), p2);
		pobr::crosspoint(_hlines[before_last], _vlines.front(), p3);
		pobr::crosspoint(_hlines[before_last], _vlines.back(), p4);
	}
	contour3d.push_back(p1);
	contour3d.push_back(p2);
	contour3d.push_back(p3);
	contour3d.push_back(p4);

	setExpirienced3D(contour3d);

	return true;
}

bool Szachownica::_findDefinition()
{
	if (_is_definision_from_exp)
		return true;

	Przedmiot_properties definition;
	contour2f_t exp_3d;

	if (!Przedmiot3D::getExpirienced3D(exp_3d))
		return false;

	double width = 0;
	cv::Point2f lb;

	for (int i = 0; i < exp_3d.size(); ++i) {
		if (i + 1 < exp_3d.size()) {
			if (width < fabs(exp_3d[i].x - exp_3d[i + 1].x)) {
				width = fabs(exp_3d[i].x - exp_3d[i + 1].x);
				//lb =  (exp_3d[i].x < exp_3d[i+1].x) ? exp_3d[i] : exp_3d[i+1];
			};
		}
	}
	width /= 3;
	lb = cv::Point2f(10.0, width + 10.0);

	definition.contour2D.clear();
	definition.contour2D.push_back(cv::Point2f(lb.x + width, lb.y - width));
	definition.contour2D.push_back(cv::Point2f(lb.x, lb.y - width));
	definition.contour2D.push_back(lb);
	definition.contour2D.push_back(cv::Point2f(lb.x + width, lb.y));

	setDefinition(definition);
	_is_definision_from_exp = true;

	return true;
}

bool Szachownica::_findMatrix3DTo2D() {

	if (is_matrix())
		return true;
	contour2f_t exp3d;
	Przedmiot_properties definitions;

	if (!getDefinition(definitions))
		return false;

	if (!is_expirienced3D())
		return false;

	getExpirienced3D(exp3d);

	if (exp3d.size() < 4 || definitions.contour2D.size() != exp3d.size())
		return false;

	cv::Point2f exp[4];
	cv::Point2f def[4];

	for (int i = 0; i < exp3d.size() && i < 4; ++i) {
		exp[i] = exp3d[i];
		def[i] = definitions.contour2D[i];
	}

	setMatrix3DTo2D(getPerspectiveTransform(exp, def));

	return true;
}

cv::Size Szachownica::_findSizeOfScene(const lines_t & hought_lines)
{
	cv::Size size(0, 0);
	for (auto& l : hought_lines) {
		if (l[0] > size.width)  size.width = l[0]; // max_x
		if (l[1] > size.height) size.height = l[1];	// max_y
		if (l[2] > size.width)  size.width = l[2];	// max_x
		if (l[3] > size.height) size.height = l[3];	// max_y
	}
	MB_Check((size.area() > 100), "size = " << size);
	CV_Assert(true);
	return size;
}


void Szachownica::__find_Chessboard_lines(const lines_t & src_lines, lines_t & dst_vlines, lines_t & dst_hlines, cv::Mat& warpMatrix,
	const cv::Size & size, const double & alpha_v, const double alpha_h)
{
	for (auto& l : src_lines)
	{
		if (pobr::is_verticla_board_line(l, alpha_v)) {
			dst_vlines.push_back(l);
		}
		else if (pobr::is_horizontal_board_line(l, alpha_h)) {
			dst_hlines.push_back(l);
		}
	}
	find_best_Vlines(dst_vlines, size);
	find_best_Hlines(dst_hlines, dst_vlines, warpMatrix, size);
}

void Szachownica::find_best_Vlines(lines_t& vlines, const cv::Size& size)
{
	pobr::extend_lines(vlines, size.width, size.height);
	//for (auto& l : _vlines)
	//	line(src, l, Scalar(0, 0, 0), 1, 8);
	vlines = pobr::group_average(vlines, size.width, size.height, 5, 10);
	//for (auto& l : _vlines)
	//	line(src, l, Scalar(255, 127, 0), 1, 8);
	vlines = pobr::prun_verticla_lines(vlines, size.width, size.height);
	vlines = pobr::find_chessboard_vectical_lines(vlines, size.width, size.height);
}

void Szachownica::find_best_Hlines(lines_t& hlines, const lines_t& vlines, cv::Mat& warpMatrix, const cv::Size& size)
{
	try {
		pobr::extend_lines(hlines, size.width, size.height);
		hlines = pobr::group_average(hlines, size.width, size.height, 5, 100);
		//for (auto& l : _hlines)
		//	line(src, l, Scalar(255, 0, 0), 1, 8);
		hlines = pobr::find_chessboard_horizontal_lines(hlines, vlines, warpMatrix, size.width, size.height);
	}
	catch (std::exception e) {
		std::cout << e.what() << std::endl;
	};
}
// ==================== scale ===================================

void Szachownica::scaleDefinition(const double & scale) {
	PrzedmiotSceny::scaleDefinition(scale);
}

// ============== draw ===========================================

cv::Mat Szachownica::__draw_Chessboard(const lines_t & vlines, const lines_t & hlines, const cv::Mat & background)
{
	cv::Mat _img_szachy = background.clone();
	for (auto l : vlines)
		pobr::line(_img_szachy, l, Scalar(0, 127, 127));
	for (auto l : hlines)
		pobr::line(_img_szachy, l, Scalar(0, 127, 127));

	return _img_szachy;
}

void Szachownica::drawFound(cv::Mat & background) const
{
	
	PrzedmiotSceny::drawFound(background, cv::Scalar(255, 255, 255), CV_FILLED);

	if (_contour_found.size() == 4) {
		double width = abs(_contour_found[0].x - _contour_found[1].x);
		//if(width < 1)
		//	width = abs(_contour_found[1].x - _contour_found[2].x);
		double blockSize = double(width / 8.0);
		
		unsigned char color = 25;
		for (double i = 0; i<width; i = i + blockSize) {
			color = ~color;
			for (double j = 0; j<width; j = j + blockSize) {
				Mat ROI = background(Rect(_contour_found[1].x+i, _contour_found[1].y+j, blockSize, blockSize));
				ROI.setTo(Scalar::all(color));
				color = ~color;
			}
		}
	}
	PrzedmiotSceny::drawFound(background, cv::Scalar(0, 0, 0), 4);
}

void Szachownica::draw2D(cv::Mat & background) const
{
	PrzedmiotSceny::draw2D(background);
}

void Szachownica::draw3D(cv::Mat & background) const
{
	//pobr::lines(background, _vlines, Scalar(255, 127, 127));
	//pobr::lines(background, _hlines, Scalar(127, 255, 127));
	PrzedmiotSceny::draw3D(background);
}

void Szachownica::drawDefined(cv::Mat & background) const
{
	PrzedmiotSceny::drawDefined(background);
}


// ============== constructors ===================================

Szachownica::Szachownica(const lines_t & hought_lines) : _alpha_v(25), _alpha_h(1), _is_definision_from_exp(false),
PrzedmiotSceny(szachownica_properties)
{
	setChessboardLines(hought_lines);
}

Szachownica::Szachownica(Reactor & zdjecie) : _alpha_v(25), _alpha_h(1),
PrzedmiotSceny(szachownica_properties)
{
	setChessboardLines(zdjecie);
}

Szachownica::~Szachownica()
{
}
