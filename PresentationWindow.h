#pragma once
#include "FiguryFinder.h"
#include "MyWindow.h"
#include "Mask.h"
#include "Szachownica.h"
#include "Klocek.h"
#include "Walec.h"

struct Scena {
	FiguryFinder* _zdjecie;
	Szachownica s;
	Klocek zielony;
	Klocek czerwony;
    Walec walec;
	cv::Mat matrix3Dto2D;
	bool _is_matrix;
};

class PresentationWindow:
	public MyWindow
{
protected:
	Scena scena[3];

	vector<HSV_setings> _masks_s;

	cv::Mat screen_1line[3];
	cv::Mat screen_2line[3];

public:
	PresentationWindow(FiguryFinder& _zdjecie1, FiguryFinder& _zdjecie2, FiguryFinder& _zdjecie3, const std::string& name = "Projekt POBR");
	~PresentationWindow();

	// Inherited via MyWindow
	virtual void do_callback() override;
	virtual void onWindowsChanged() override;
	void scaleFigury(const double & scale);
	void process();
	void findFigury();
	void findFigurySceny(Scena & s);
	void findSzachownica(Scena & s, const int & i);
	void findSzachownica(Scena & s);
	void findKlocek(Scena & s, Klocek & k, const int & i);
	void findWalec(Scena & s);
	void preProcess(FiguryFinder * f);
	void process(FiguryFinder * f, const int & i);
	void redraw_scena(const int & i);
	void refresh_window();
};

