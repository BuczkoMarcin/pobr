#pragma once
#include "segment_labels.h"

class Gradient_segmentation
{
	typedef  std::pair<int, int> t_Point;

	Segment_labels _labels;
	std::list<t_Point> grad_seed;
	cv::Mat_<cv::Vec3b>* _picture;
	double _gradient_step;
	double _labels_number;

	bool is_gradient(const cv::Vec3b& A, const cv::Vec3b B);
	void find_gradient(const Segment_labels::t_Label& label, const int & x, const int & y);
	void clear();
public:
	Gradient_segmentation(cv::Mat_<cv::Vec3b>& picture, const double& gradient_step = 0.1);
	~Gradient_segmentation();
	//! wykonuje segmetacje gradj�tow�
	void operator()();
	//! koloruje segmenty
	void color_segments();
	//! zwraca ilosc znalezionych segmentow
	int getNumebOfLabels();
	

};

