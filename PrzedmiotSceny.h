#pragma once
#include "Przedmiot2D.h"
#include "Przedmiot3D.h"
class PrzedmiotSceny :
	protected Przedmiot2D, protected Przedmiot3D
{
	cv::Mat _3DTo2DMatrix;
	bool _is_matrix;

protected:
	bool create2DExpirience();
	void reset();
	bool is_matrix() const { return _is_matrix; };

public:
	//zwraca macierz przekszta�ce� z 3D zdj�cia na 2D p�aszczyzn�
	bool getMatrix3DTo2D(cv::Mat& matrix) const;

	void scaleDefinition(const double & scale);

	// ustawia contour uzyskany ze zdj�cia
	void setExpirienced3D(const contour2f_t& contour3d_exp);

	// ustawia macierz przekszta�cenia z 3D zdj�cia na 2D p�aszczyzn�
	void setMatrix3DTo2D(const cv::Mat& matrix);

	// Znajduje przedmiot o ile ma ustawiony contur3d, definicj�, i macierz przekszta�cenia w 2D
	bool findPrzedmiot();

	PrzedmiotSceny(const Przedmiot_properties& definition);
	PrzedmiotSceny(const Przedmiot_properties& definition, const contour2f_t& contour3d_exp);
	PrzedmiotSceny(const Przedmiot_properties& definition, const contour2f_t& contour3d_exp,  const cv::Mat& _3DTo2DMatrix);
};

