#include "PrezentacjaZHVS.h"



PrezentacjaZHVS::PrezentacjaZHVS(FiguryFinder & zdjecie1, FiguryFinder & zdjecie2, FiguryFinder & zdjecie3, const std::string& name):
	MyWindow(zdjecie1, name, trackbar_vect_t({
			{ 0,255,"h: min" },
			{ 9,255,"h: max" },
			{ 172,255,"s: min" },
			{ 255,255,"s: max" },
			{ 0,255,"v: min" },
			{ 255,255,"v: max" },
			{ 0,4, "screen" } })),
		PresentationWindow(zdjecie1,zdjecie2,zdjecie3,name),
		_last_mask_index(-1)
{
	cv::Size size = PresentationWindow::_img->getImage().size();
	double height = 400;
	cv::resizeWindow(PresentationWindow::_name, (size.width * double(height / (2 * size.height)))*3.0, height);
	refresh_window();
}


PrezentacjaZHVS::~PrezentacjaZHVS()
{
}

void PrezentacjaZHVS::do_callback()
{

	if (_last_mask_index != MyWindow::_trackbars[6].value &&  MyWindow::_trackbars[6].value == 0) {
		set_MaskSettings_From_Trackbars();

	}
	else if (_last_mask_index != MyWindow::_trackbars[6].value &&  MyWindow::_trackbars[6].value != 0) {
		set_Trackbars_From_MaskSettings();
	}
	else if (_last_mask_index == MyWindow::_trackbars[6].value &&  MyWindow::_trackbars[6].value != 0) {
		set_MaskSettings_From_Trackbars();
	}
	else {

	}
	_last_mask_index = MyWindow::_trackbars[6].value;

	PrezentacjaZHVS::process();
	//redraw_scena(0);
	//redraw_scena(1);
	//redraw_scena(2);
	refresh_window();
}

void PrezentacjaZHVS::onWindowsChanged()
{
}


void PrezentacjaZHVS::process() {
	for (size_t i = 0; i < 3; ++i) {
		preProcess(scena[i]._zdjecie);
		PresentationWindow::process(scena[i]._zdjecie, i);
		//findFigurySceny(scena[i]);
		processMasks(scena[i]._zdjecie, i, MyWindow::_trackbars[6].value -1);
	};
}

void PrezentacjaZHVS::processMasks(FiguryFinder* f, const int& screen, const int& mask) {
	if (mask < 0 || mask >= 3)
		return;
	cv::Mat masked = f->getMasked_F(mask);
	if (screen<3)
		screen_1line[screen] = masked.clone();
}

// =====================Trackbars to masks  ================

void PrezentacjaZHVS::set_MaskSettings_From_Trackbars()
{
	HSV_setings tR_HSV = {
		MyWindow::_trackbars[0].value,
		MyWindow::_trackbars[1].value,
		MyWindow::_trackbars[2].value,
		MyWindow::_trackbars[3].value,
		MyWindow::_trackbars[4].value,
		MyWindow::_trackbars[5].value
	};
	if (!setMask_s(MyWindow::_trackbars[6].value - 1, tR_HSV)) {
		setMask_s(0, Mask::RED);
		setMask_s(1, Mask::GREEN);
		setMask_s(2, Mask::CYLINDER);
		setMask_s(3, Mask::NULL_MASK);
	}
}

void PrezentacjaZHVS::set_Trackbars_From_MaskSettings()
{
	HSV_setings mask_HSV;
	if (!getMask_s(MyWindow::_trackbars[6].value - 1, mask_HSV)) return;

	MyWindow::_trackbars[0].value = mask_HSV.min_h;
	MyWindow::_trackbars[1].value = mask_HSV.max_h;
	MyWindow::_trackbars[2].value = mask_HSV.min_s;
	MyWindow::_trackbars[3].value = mask_HSV.max_v;
	MyWindow::_trackbars[4].value = mask_HSV.min_v;
	MyWindow::_trackbars[5].value = mask_HSV.max_v;

	cv::setTrackbarPos(MyWindow::_trackbars[0].name, MyWindow::_name, MyWindow::_trackbars[0].value);
	cv::setTrackbarPos(MyWindow::_trackbars[1].name, MyWindow::_name, MyWindow::_trackbars[1].value);
	cv::setTrackbarPos(MyWindow::_trackbars[2].name, MyWindow::_name, MyWindow::_trackbars[2].value);
	cv::setTrackbarPos(MyWindow::_trackbars[3].name, MyWindow::_name, MyWindow::_trackbars[3].value);
	cv::setTrackbarPos(MyWindow::_trackbars[4].name, MyWindow::_name, MyWindow::_trackbars[4].value);
	cv::setTrackbarPos(MyWindow::_trackbars[5].name, MyWindow::_name, MyWindow::_trackbars[5].value);

}

bool PrezentacjaZHVS::setMask_s(const int & i, const HSV_setings& mask_s)
{
	if (i < 0 || i >= _masks_s.size())
		return false;
	_masks_s[i] = mask_s;
	return true;
}

bool PrezentacjaZHVS::getMask_s(const int & i, HSV_setings& mask_s)
{
	if (i < 0 || i >= _masks_s.size())
		return false;
	mask_s = _masks_s[i];
	return true;
}

// ===========================================================