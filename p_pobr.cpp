#pragma once
#include "MaskWindow.h"
#include "ReactorWindow.h"
#include "FiguryWindow.h"
#include "HoughWindow.h"
#include "FiguryFinder.h"
#include "SzachyWindowTest.h"
#include "Przedmiot2DWindowTest.h"
#include "PresentationWindow.h"
#include "PrezentacjaZHVS.h"
#include "CannyWindow.h"

using namespace cv;

int main()
{
	try {

		FiguryFinder zdjecie1("s1.jpg");
		FiguryFinder zdjecie2("s2.jpg");
		FiguryFinder zdjecie3("s3.jpg");
		FiguryFinder zdjecie4("s4.jpg");
		FiguryFinder zdjecie5("s5.jpg");
		FiguryFinder zdjecie6("s6.jpg");


		PresentationWindow presentationWindow1(zdjecie1, zdjecie2, zdjecie3, "POBR I");
		PresentationWindow presentationWindow2(zdjecie4, zdjecie5, zdjecie6, "POBR II");

		//PrezentacjaZHVS prezentacjaZHVS1(zdjecie1, zdjecie2, zdjecie3, "POBR");
		//PrezentacjaZHVS prezentacjaZHVS2(zdjecie4, zdjecie5, zdjecie6, "Projekt POBR II");

		//Przedmiot2DWindowTest przedmiot2DWindowTest(zdjecie);

		//CannyWindow cannyWindow(zdjecie1);
		//MaskWindow maskWindow(zdjecie1);
		//HoughWindow houghWindow(zdjecie1);

		ReactorWindow reactorWindow(zdjecie1);
		//MaskWindow maskWindow(zdjecie1);
		//FiguryWindow figuryWindow(zdjecie);
		//SzachyWindowTest szachownicaWindow(zdjecie);

		//FiguryFinder reactor("s1.jpg");
		//MaskWindow maskWindow(reactor);
		//ReactorWindow reactorWindow(reactor);
		//FiguryWindow figuryWindow(reactor);

		//findcircle();

		cv::waitKey();
	}
	catch (std::exception e) {
		cout << e.what() << endl;
	}
	int a;
	cin >> a;
	cv::waitKey();
	return 0;
}
