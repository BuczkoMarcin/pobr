#include "contours_utils.h"


cv::Point2f centerOfMass(const std::vector<cv::Point2f>& contour)
{
	cv::Moments mu;
	if (contour.size() == 0)
		return cv::Point2f(0.0, 0.0);
	if (contour.size() <= 2) {
		double x = 0;
		double y = 0;
		for (size_t i=0; i < contour.size(); i++) {
			x += contour[i].x;
			y += contour[i].y;
		}
		x /= contour.size();
		y /= contour.size();
		return cv::Point2f(x, y);

	}

	mu = cv::moments(contour);
	if(mu.m00!=0)
		return cv::Point2f(float(mu.m10 / mu.m00), float(mu.m01 / mu.m00));
	return cv::Point2f(0.0, 0.0);
}

// obr�t punktu p wok� punktu center o k�t angle (w radianach)
cv::Point2f rotate(const double& angle, const cv::Point2f& center, const cv::Point2f& p)
{
	cv::Point2f r;
	r.x = ((p.x - center.x) * std::cos(angle)) - ((p.y - center.y) * std::sin(angle)) + center.x;
	r.y = ((p.x - center.x) * std::sin(angle)) + ((p.y - center.y) * std::cos(angle)) + center.y;
	return r;
}

//obr�t contouru wok� punktu center o k�t angle (w radianach)
void rotate(const double & angle, const cv::Point2f& center, std::vector<cv::Point2f>& contour)
{
	for (auto& p : contour)
		p = rotate(angle, center, p);
}

void move(const cv::Point2f& vector_shift, contour2f_t& contour)
{
	for (auto& p : contour)
		p += vector_shift;
}


void  drawContour2f(cv::Mat& img, const std::vector<cv::Point2f>& contour, const cv::Scalar& color, const int& thickness) {
	std::vector<cv::Point> c2i = contour2f_to_2i(contour);
	std::vector<std::vector<cv::Point> > contourVec;
	contourVec.push_back(c2i);
	cv::drawContours(img, contourVec, 0, color, thickness, 8);
}

void scaleConture(std::vector<cv::Point2f>& contour, const double& scale) {
	for (auto& p : contour)
		p *= scale;
}


std::vector<cv::Point>  contour2f_to_2i(const std::vector<cv::Point2f>& contour) {
	std::vector<cv::Point> out;
	for (cv::Point2f p : contour)
		out.push_back(cv::Point(p));
	return out;
}

std::vector<cv::Point2f>  contour2i_to_2f(const std::vector<cv::Point>& contour) {
	std::vector<cv::Point2f> out;
	for (cv::Point p : contour)
		out.push_back(cv::Point2f(p));
	return out;
}
