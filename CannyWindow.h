#pragma once
#include "MyWindow.h"

class CannyWindow: public MyWindow  {
	
protected:
	void do_callback();
	void onWindowsChanged();
	void refresh_window();

	CannyWindow(Zdjecie & img, const string & window_name, 
				const bool & external_trackbars = false, 
				const string & trackbar_window_name = "Trackbars");
public:
	CannyWindow(Zdjecie& img);
};