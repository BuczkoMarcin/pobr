#include "CannyWindow.h"

using namespace cv;

CannyWindow::CannyWindow(Zdjecie & img, const string & window_name,
	const bool& external_trackbars, const string& trackbar_window_name) :
	MyWindow(img, window_name, trackbar_vect_t({
		{ 10, 1000, "th1" },
		{ 100, 1000, "th2" }}),
		external_trackbars,
		trackbar_window_name)
{
	do_callback();
}

CannyWindow::CannyWindow(Zdjecie& img): CannyWindow(img, "Canny")
{
};

void CannyWindow::do_callback() {
	_img->setCanny({ double(_trackbars[0].value), double(_trackbars[1].value) });
	_img->doCanny();
	refresh_window();
	//_img->imgShow("src");
	//cout << "CANNY WINDOW CALL" << endl;
}

void CannyWindow::onWindowsChanged()
{
	refresh_window();
	//cout << this << " CannyWindow got signal" << endl;
}

void CannyWindow::refresh_window()
{
	_img->cannyShow(_name);
}


