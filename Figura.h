#pragma once
#include "opencv2\opencv.hpp"
#include "Mask.h"

class Figura: public Mask
{

public:
	Figura(const cv::Mat& img, const cv::Mat& mask, const Mask_type& type);
	~Figura();
};

