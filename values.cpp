#include "pobr_types.h"
#include "Mask.h"

Przedmiot_properties szachownica_properties{
	{
	cv::Point2f(190.0,10.0),
	cv::Point2f(10.0,10.0),
	cv::Point2f(10.0,190.0),
	cv::Point2f(190.0,190.0) 
	}, 0.0,  cv::Scalar(255,255,255)
};

//Przedmiot_properties szachownica_properties{
//	{
//	cv::Point2f(190.0,10.0),
//	cv::Point2f(10.0,10.0),
//	cv::Point2f(10.0,190.0),
//	cv::Point2f(190.0,10.0)
//	}, 0.0,  cv::Scalar(255,255,255)
//};


Przedmiot_properties walecProperties{
	{}, 12.5,  cv::Scalar(111,182,231)
};



contour2f_t testContours = {
	cv::Point2f(50.0,250.0),
	cv::Point2f(250.0,250.0),
	cv::Point2f(250.0,50.0),
	cv::Point2f(50.0,50.0) };

Przedmiot_properties testProperties = { testContours, 0.0, cv::Scalar(255,0,0) };

int zielonyKlocek_width = 25.0;
contour2f_t zielonyKlocekContour = {
	cv::Point2f(0.0,zielonyKlocek_width),
	cv::Point2f(zielonyKlocek_width,zielonyKlocek_width),
	cv::Point2f(zielonyKlocek_width,0.0),
	cv::Point2f(0.0,0.0) };

Przedmiot_properties zielonyKlocekProperties = { zielonyKlocekContour, zielonyKlocek_width*0.90, cv::Scalar(41,143,39) };

int czerwonyKlocek_width_y = 14.0;
int czerwonyKlocek_width_x = 55.0;
int czerwonyKlocek_height = 20.0;
contour2f_t czerwonyKlocekContour = {
	cv::Point2f(0.0,czerwonyKlocek_width_x),
	cv::Point2f(czerwonyKlocek_width_y,czerwonyKlocek_width_x),
	cv::Point2f(czerwonyKlocek_width_y,0.0),
	cv::Point2f(0.0,0.0) };
Przedmiot_properties czerwonyKlocekProperties = { czerwonyKlocekContour, czerwonyKlocek_height, cv::Scalar(36,60,239) };


const HSV_setings Mask::RED = { 0, 6,    174, 255,  174, 255 };
const HSV_setings Mask::GREEN = { 27, 33,  81, 180,    112, 180 };
const HSV_setings Mask::CYLINDER = { 10, 15,  175, 200,  12, 180 };
const HSV_setings Mask::WHITE_SQUARES = { 9, 30,   0, 255,    0, 227 };
const HSV_setings Mask::BLACK_SQUARES = { 9, 30,   0, 255,    0, 118 };
const HSV_setings Mask::BACKGROUND = { 20, 30,  0, 255,    118, 255 };
const HSV_setings Mask::NULL_MASK = { 0, 255,  0, 255,    0,  255 };

// punkt zbiegu pionowego  
cv::Point2f h_vpoint(300.0, 1000.0);
