#pragma once
#include "ZdjecieZFiltrem.h"
class Reactor :
	public ZdjecieZFiltrem
{
protected:
	cv::Mat _result;

public:
	void reset();
	void IlluminationNormalization();
	const cv::Mat doFilter(const bool & normalizeIlumination = false);
	void IlluminationCorrection();
	const cv::Mat doCanny();
	const cv::Mat doHough(lines_t& lines);
	const cv::Mat doHough();
	const cv::Mat getResult();
	void resultShow(const std::string& window_name);

	Reactor(const Reactor& _reactor) : ZdjecieZFiltrem(_reactor) { _result = _result.clone(); }
	Reactor(const cv::Mat& src) : ZdjecieZFiltrem(src) { _result = _src.clone(); };
	Reactor(const std::string& file_name) : ZdjecieZFiltrem(file_name) { _result = _src.clone(); };
};

