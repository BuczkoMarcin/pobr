#include "PrzedmiotSceny.h"

// ============= find create =====================================

bool PrzedmiotSceny::findPrzedmiot()
{
	if (!create2DExpirience())
		return false;
	if (!Przedmiot2D::findPrzedmiot())
		return false;
	return true;
}

bool PrzedmiotSceny::create2DExpirience()
{
	if ( is_expirienced2D() )
		return true;

	if (! _is_matrix )
		return false;

	if (! is_expirienced3D() )
		return false;

	contour2f_t exp_2d;
	contour2f_t exp_3d;
	
	getExpirienced3D(exp_3d);

	try {
		cv::perspectiveTransform(exp_3d/*src*/, exp_2d/*dst*/, _3DTo2DMatrix);
	}
	catch (std::exception e) {
		std::cout << e.what() << std::endl;
	}
	//std::cout << "lexp_3d :";
	//for (auto & p : exp_3d)
	//	std::cout << p  << " ";
	//std::cout << std::endl;
	//std::cout << "lexp_2d :";
	//for (auto & p : exp_2d)
	//	std::cout << p << " ";
	//std::cout << std::endl;
	Przedmiot2D::setExpirienced2D(exp_2d);
	return true;
}

// ============= reset ===========================================

void PrzedmiotSceny::reset()
{
	_is_matrix = false;
	Przedmiot2D::reset();
	Przedmiot3D::reset();
}

// ============= get set =========================================

void PrzedmiotSceny::setExpirienced3D(const contour2f_t & contour_exp)
{
	Przedmiot2D::reset();
	Przedmiot3D::reset();
	Przedmiot3D::setExpirienced3D(contour_exp);
	//create2DExpirience();
}

void PrzedmiotSceny::setMatrix3DTo2D(const cv::Mat & matrix)
{

	Przedmiot2D::reset();
	_3DTo2DMatrix = matrix.clone();
	_is_matrix = true;
	//create2DExpirience();
}

bool PrzedmiotSceny::getMatrix3DTo2D(cv::Mat & matrix) const 
{
	if (!_is_matrix)
		return false;
	matrix = _3DTo2DMatrix.clone();
	return true;
}

// ==================== scale =======================================

void PrzedmiotSceny::scaleDefinition(const double & scale)
{
	Przedmiot2D::scaleDefinition(scale);
	_is_matrix = false;
};

// ============= Constructors =====================================


PrzedmiotSceny::PrzedmiotSceny(const Przedmiot_properties & definition): Przedmiot2D(definition)
{
	_is_matrix = false;
}

PrzedmiotSceny::PrzedmiotSceny(const Przedmiot_properties & definition, const contour2f_t & contour3d_exp): PrzedmiotSceny(definition)
{
}

PrzedmiotSceny::PrzedmiotSceny(const Przedmiot_properties & definition, const contour2f_t & contour3d_exp,  const cv::Mat & matrix3DTo2D)
	:PrzedmiotSceny(definition, contour3d_exp)
{
	setMatrix3DTo2D(matrix3DTo2D);
}
