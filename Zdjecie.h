#pragma once
#include "opencv2\opencv.hpp"
#include "pobr_types.h"

class Zdjecie
{

protected:

	cv::Mat _src;

	bool _is_canny;
	bool _is_hough;

	cv::Mat __doCanny(const cv::Mat& src);
	lines_t __doHough(const cv::Mat& src);
	cv::Mat __doHoughImg(const lines_t& lines, const cv::Mat& backgrond);

private:

	Canny_setings _c;
	Hough_setings _h;
	cv::Mat _canny;
	cv::Mat _hough;
	bool _is_hough_img_modified;
	lines_t _hough_lines;

	void doHoughImg();
	void _doHough_in_squers(const unsigned& x_factor, const unsigned& y_factor);
	void _doHough_in_squer(const cv::Rect& region);
	void Init();

public:
	Zdjecie(const Zdjecie& _zdjecie);
	Zdjecie(const cv::Mat& src);
	Zdjecie(const std::string& file_name);
	~Zdjecie();

	void setHough(const Hough_setings& h);
	void setCanny(const Canny_setings& c);

	void doCanny();
	void doCanny(cv::Mat& dst);

	void doHough();
	void doHough(lines_t& dst);

	void getImage(cv::Mat& dst);
	cv::Mat getImage();
	bool getCanny(cv::Mat& dst);
	bool getHough(lines_t& dst) const;
	void imgShow(std::string window_name);
	void cannyShow(std::string window_name);
	void houghShow(std::string window_name);

	cv::Size getSize();
	int getType();
};

