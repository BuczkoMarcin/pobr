#include "SzachyWindowTest.h"
#include "pobr_types.h"
#include "utils.h"


SzachyWindowTest::SzachyWindowTest(FiguryFinder & img) :
	MyWindow(img, "Szachownica",
		trackbar_vect_t({
			{ 0, 1, "original" },
			{ 0, 1, "definition" },
			{ 0, 1, "3d exp" },
			{ 0, 1, "2d exp" },
			{ 0, 1, "found" },
			{ 0, 1, "h lines" }
}), true, "Szachownica settings"),
_img_szachy(img.getSize(), img.getType(), 0),
_show_org_img(false),
_show_3dContours(false),
_show_2dContours(false),
_show_Found(false),
_show_definition(false),
_show_h_lines(false),
_is_matrix(false),
s(img),
zielony(zielonyKlocekProperties),
czerwony(czerwonyKlocekProperties),
walec(walecProperties)
{
	f = (FiguryFinder*)_img;
	onWindowsChanged();
}




SzachyWindowTest::~SzachyWindowTest()
{
}

void SzachyWindowTest::do_callback()
{
	set_flags_on_callback();
	if (_show_org_img)
		_img_szachy = f->getResult();
	else
		_img_szachy = Mat::zeros(_img_szachy.size(), _img_szachy.type());
	if (_show_Found) {
		s.drawFound(_img_szachy);
		zielony.drawFound(_img_szachy);
		czerwony.drawFound(_img_szachy);
		walec.drawFound(_img_szachy);
	}
	if (_show_definition) {
		s.drawDefined(_img_szachy);
		zielony.drawDefined(_img_szachy);
		czerwony.drawDefined(_img_szachy);
		walec.drawDefined(_img_szachy);
	};
	if (_show_3dContours) {
		s.draw3D(_img_szachy);
		zielony.draw3D(_img_szachy);
		czerwony.draw3D(_img_szachy);
		walec.draw3D(_img_szachy);
	}
	if (_show_2dContours) {
		s.draw2D(_img_szachy);
		zielony.draw2D(_img_szachy);
		czerwony.draw2D(_img_szachy);
		walec.draw2D(_img_szachy);
	}

	if (_show_h_lines) {
		pobr::lines(_img_szachy, h_lines, cv::Scalar(255, 0, 0));
	}
	imshow(MyWindow::_name, _img_szachy);
}

void SzachyWindowTest::onWindowsChanged()
{
	try {

		findSzachownica();
		findKlocek(zielony,1);
		findKlocek(czerwony,0);
		findWalec();
	}
	catch (std::exception e) {
		cout << e.what() << endl;
	}


	do_callback();
}


void SzachyWindowTest::findWalec() {
	if (!_is_matrix)
		return;
	walec.setMatrix3DTo2D(matrix3Dto2D);
	f->resetMasked_F();
	f->findContours();
	contours_t contoures = f->getConture_F(2).contours;
	if (contoures.size() > 0) {
		walec.setExpirienced3D(contour2i_to_2f(f->getConture_F(2).contours[0]));
	};
	walec.findPrzedmiot();
}

void SzachyWindowTest::findKlocek(Klocek & k, const int& i ) {
	if (!_is_matrix)
		return;
	k.setMatrix3DTo2D(matrix3Dto2D);
	f->resetMasked_F();
	f->findContours();
	contours_t contoures = f->getConture_F(i).contours;
	if (contoures.size() > 0) {
		k.setExpirienced3D(  contour2i_to_2f( f->getConture_F(i).contours[0] )  );
	};
	k.findPrzedmiot();
}

void SzachyWindowTest::findSzachownica() {
	Reactor r(*f);
	r.doCanny();
	r.doHough(h_lines);
	s.setChessboardLines(h_lines);
	s.find_Chessboard();
	_is_matrix = false;
	if (s.getMatrix3DTo2D(matrix3Dto2D))
		_is_matrix = true;
}


void SzachyWindowTest::set_flags_on_callback()
{
	_show_org_img = (_trackbars[0].value == 1) ? true : false;
	_show_definition = (_trackbars[1].value == 1) ? true : false;
	_show_3dContours = (_trackbars[2].value == 1) ? true : false;
	_show_2dContours = (_trackbars[3].value == 1) ? true : false;
	_show_Found = (_trackbars[4].value == 1) ? true : false;
	_show_h_lines = (_trackbars[5].value == 1) ? true : false;
}
