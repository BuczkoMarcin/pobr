#include "FiguryWindow.h"



//FiguryWindow::FiguryWindow()
//{
//}



void FiguryWindow::onWindowsChanged()
{
	try {
		_img_F->resetMasked_F();
		_img_F->findContours();
		refreshScreeen_F(true);
		show_screen();

	}
	catch (std::exception e) {
		cout << e.what() << endl;
	}
}

void FiguryWindow::do_Quadruple_callback()
{
	refreshScreeen_F(true);
}

void FiguryWindow::refreshScreeen_F(const bool & all)
{
	_refreshScreeen_F(1);
	_refreshScreeen_F(2);
	_refreshScreeen_F(3);
	if (all) _refreshScreeen_F(4);
}

void FiguryWindow::_refreshScreeen_F(int screen_nr)
{
	try {
		//_img_F->_findContours(screen_nr - 1);
		cv::Mat masked;
		_img_F->getMasked(screen_nr - 1, masked);
		cv::Mat contured = _img_F->_doFiguraImg(screen_nr - 1, masked);

		switch (screen_nr) {
		case 1: setScreen_1(contured); break;
		case 2: setScreen_2(contured); break;
		case 3: setScreen_3(contured); break;
		case 4: setScreen_4(contured); break;
		default: return;
		}
	}
	catch (std::exception e) {
		cout << e.what() << endl;
	}
}
