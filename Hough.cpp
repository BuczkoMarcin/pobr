//#include "opencv2/highgui/highgui.hpp"
//#include "opencv2/imgproc/imgproc.hpp"
#include <opencv2/core/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/calib3d/calib3d.hpp>
#include <opencv2/highgui/highgui.hpp>

#include <iostream>

using namespace cv;
using namespace std;

void help()
{
	cout << "\nThis program demonstrates line finding with the Hough transform.\n"
		"Usage:\n"
		"./houghlines <image_name>, Default is pic1.jpg\n" << endl;
}

void find() {


	Size patternsize(8, 8); //interior number of corners
	const char* filename = "szachy_1_color.jpg";
	Mat gray = imread(filename, 0);
	Mat img;
	vector<Point2f> corners; //this will be filled by the detected corners

							 //CALIB_CB_FAST_CHECK saves a lot of time on images
							 //that do not contain any chessboard corners
	bool patternfound = findChessboardCorners(gray, patternsize, corners,
		CALIB_CB_ADAPTIVE_THRESH + CALIB_CB_NORMALIZE_IMAGE
		+ CALIB_CB_FAST_CHECK);

	if (patternfound)
		cornerSubPix(gray, corners, Size(11, 11), Size(-1, -1),
			TermCriteria(CV_TERMCRIT_EPS + CV_TERMCRIT_ITER, 30, 0.1));

	drawChessboardCorners(img, patternsize, Mat(corners), patternfound);

	imshow("1", gray);
	imshow("2", img);


	waitKey();

}

struct line_ab {
	double a;
	double b;
	double vert_x;
	bool perfect_vertical;
};
void find_all_corsspoints(const Vector<Vec4i> lines, Vector<Point*>& crosspoints);
bool crosspoint(const line_ab& L, const line_ab& K, Point& crosspoint_);
void transform_ab(const Vec4i& line, line_ab& lineab);

bool isEqual(const Vec4i& _l1, const Vec4i& _l2)
{
	Vec4i l1(_l1), l2(_l2);

	float length1 = sqrtf((l1[2] - l1[0])*(l1[2] - l1[0]) + (l1[3] - l1[1])*(l1[3] - l1[1]));
	float length2 = sqrtf((l2[2] - l2[0])*(l2[2] - l2[0]) + (l2[3] - l2[1])*(l2[3] - l2[1]));

	float product = (l1[2] - l1[0])*(l2[2] - l2[0]) + (l1[3] - l1[1])*(l2[3] - l2[1]);

	if (fabs(product / (length1 * length2)) < cos(CV_PI / 30))
		return false;

	float mx1 = (l1[0] + l1[2]) * 0.5f;
	float mx2 = (l2[0] + l2[2]) * 0.5f;

	float my1 = (l1[1] + l1[3]) * 0.5f;
	float my2 = (l2[1] + l2[3]) * 0.5f;
	float dist = sqrtf((mx1 - mx2)*(mx1 - mx2) + (my1 - my2)*(my1 - my2));

	if (dist > std::max(length1, length2) * 0.5f)
		return false;

	return true;
}


int lines() {

	const char* filename = "szachy_1_prog.jpg";

	Mat src = imread(filename, 0);
	if (src.empty())
	{
		help();
		cout << "can not open " << filename << endl;
		return -1;
	}

	Mat dst, cdst2;
	Canny(src, dst, 50, 200, 3);
	Mat cdst(src.rows, src.cols, CV_AA);
	//cvtColor(dst, cdst, CV_GRAY2BGR);
	cvtColor(dst, cdst2, CV_GRAY2BGR);

#if 0
	vector<Vec2f> lines;
	HoughLines(dst, lines, 1, CV_PI / 180, 100, 0, 0);
	std::cout << std::endl << "wersja 0" << std::endl;
	for (size_t i = 0; i < lines.size(); i++)
	{
		float rho = lines[i][0], theta = lines[i][1];
		Point pt1, pt2;
		double a = cos(theta), b = sin(theta);
		double x0 = a*rho, y0 = b*rho;
		pt1.x = cvRound(x0 + 1000 * (-b));
		pt1.y = cvRound(y0 + 1000 * (a));
		pt2.x = cvRound(x0 - 1000 * (-b));
		pt2.y = cvRound(y0 - 1000 * (a));
		line(cdst, pt1, pt2, Scalar(0, 0, 255), 3, CV_AA);
	}
#else
	vector<Vec4i> lines;
	vector<Vec4i> lines_vertical;
	Vector<Point*> crosspoints;
	Vec4i cl;
	int w = 800;
	cv::Mat_<cv::Vec3b> points(w, w, CV_8UC3);
	HoughLinesP(dst, lines, 1, CV_PI / 90, 50, 50, 10);
	std::cout << std::endl << "wersja 1" << std::endl;
	std::cout << std::endl << cl << std::endl;

	std::vector<int> labels;
	int numberOfLines = cv::partition(lines, labels, isEqual);

	cout << numberOfLines << " in lines: " << labels.size() << endl;

	for (size_t i = 0; i < lines.size(); i++)
	{
		Vec4i l = lines[i];
		//std::cout << std::endl << l << std::endl;
		double dx = l[2] - l[0];
		double dy = l[3] - l[1];
		///std::cout << std::endl << dx << " " << dy  << std::endl;
		if (dx == 0 || dy / dx > 1 || dy / dx < -1) {
			line_ab abl;
			int n = 5;
			//l[1] = l[1] / n + (n - 1)*cdst.rows / n;
			//l[3] = l[3] / n + (n - 1)*cdst.rows / n;

			transform_ab(l,abl);
			Point pt1, pt2;

			
			pt1.x = 0;
			pt1.y = pt1.x * abl.a + abl.b;
			pt2.x = cdst.cols;
			pt2.y = pt2.x * abl.a + abl.b;

			//std::cout << std::endl << pt1 << " " << pt2 << std::endl;
			line(cdst, pt1, pt2, Scalar(0, labels[i] * 10 , 255), 1, CV_AA);
			//line(cdst, Point(10, 10), Point(dst.cols - 10, dst.rows - 10), Scalar(0, 255, 0), 1, CV_AA);
			lines_vertical.push_back(l);
		};
		line(cdst2, Point(l[0], l[1]), Point(l[2], l[3]), Scalar(0, 0, 255), 1, CV_AA);
		
		
		/*
		find_all_corsspoints(lines_vertical, crosspoints);


		for (auto it = crosspoints.begin(); it != crosspoints.end(); ++it)
		{
			int x = w/2+ (*it)->x/8;
			int y = w/2-(*it)->y/8;

			if (x >= 0 && x < points.rows && y >= 0 && y < points.cols) {
				points(x, y)[0] = 255;
				points(x, y)[1] = 255;
				points(x, y)[2] = 255;
			}
			//cout << (*it)->x << " " << (*it)->y << endl;
			delete (*it);

		}
		crosspoints.clear();
		*/

		

	}
	//line(cdst, Point(10, 10), Point(dst.cols - 10, dst.rows - 10), Scalar(0, 255, 0), 1, CV_AA);
	line(cdst, Point(10, dst.rows - 100 ), Point(dst.cols-10, dst.rows - 100), Scalar(0, 255, 0), 1, CV_AA);
	
#endif
	imshow("source", src);
	imshow("source II", dst);
	imshow("detected lines", cdst);
	//imshow("mmmm", points);
    imshow("detected lines II", cdst2);

	waitKey();
	return 0;
}

void transform_ab(const Vec4i& line, line_ab& lineab) {
	double dx = line[2] - line[0];
	double dy = line[3] - line[1];
	if (dx == 0) {
		lineab.perfect_vertical = true;
		lineab.vert_x = line[2];
	}
	else {
		lineab.perfect_vertical = false;
		lineab.a = dy / dx;
		lineab.b = line[1] - line[0] * lineab.a;
	}

	//std::cout << "transform_ab: " << lineab.a << " " << lineab.b << endl;
};

void find_all_corsspoints(const Vector<Vec4i> lines, Vector<Point*>& crosspoints) {
	line_ab lineab;
	std::vector<line_ab*> lines_ab;
	for (size_t i = 0; i < lines.size(); i++)
	{
		Vec4i l = lines[i];
		transform_ab(l, lineab);
		lines_ab.push_back(new line_ab(lineab));
	};

	for (size_t i = 0; i < lines.size(); i++)
	{
		for (size_t j = i; j < lines.size(); j++)
		{
			Point *crosspoint_ = new Point();
			if (crosspoint(*lines_ab[i], *lines_ab[j], *crosspoint_))
				crosspoints.push_back(crosspoint_);
			else
				delete crosspoint_;
		};
	};

	std::cout << "corss_size:" << crosspoints.size() << endl;

	for (std::vector< line_ab* >::iterator it = lines_ab.begin(); it != lines_ab.end(); ++it)
	{
		delete (*it);
	}
	lines_ab.clear();

};


bool crosspoint(const line_ab& L, const line_ab& K, Point& crosspoint_) {
	
	
	if ((L.perfect_vertical && K.perfect_vertical) || (K.a == L.a && !(L.perfect_vertical || K.perfect_vertical)))
		return false;
	
	if (K.perfect_vertical) 
	{
		crosspoint_.x = K.vert_x;
		crosspoint_.y = L.a*crosspoint_.x + L.b;
	}
	else if (L.perfect_vertical) 
	{
		crosspoint_.x = L.vert_x;
		crosspoint_.y = K.a*crosspoint_.x + K.b;
	}
	else
	{
		crosspoint_.x = (K.b - L.b) / (L.a - K.a);
		crosspoint_.y = L.a*crosspoint_.x + L.b;
	};
	//std::cout << "find_corss: " << crosspoint_.x << " " << crosspoint_.y << endl;
 	return true;
}



int main(int argc, char** argv)
{
	
	lines();
	return 0;
}