#include "Klocek.h"
#include "utils.h"
#include "contours_utils.h"


Klocek::Klocek(const contour2f_t & contour3d_exp, const cv::Mat & matrix3DTo2D):
	PrzedmiotSceny(zielonyKlocekProperties, contour3d_exp,matrix3DTo2D)

{
}

Klocek::Klocek() :
	PrzedmiotSceny(zielonyKlocekProperties)

{
}

Klocek::Klocek(const Przedmiot_properties& definition) :
	PrzedmiotSceny(definition)

{
}

void Klocek::setExpirienced3D(const contour2f_t & contour_exp) {
	
	Przedmiot_properties prop;
	cv::Mat matrix;
	if (!getDefinition(prop) || !getMatrix3DTo2D(matrix)) {
		Przedmiot3D::setExpirienced3D(contour_exp);
		return;
	}

	contour2f_t contour = contour_exp;
	contour2f_t vect_height_3d;
	contour2f_t vect_height_2d;
	vect_height_2d.push_back(cv::Point2f(0.0,0.0));
	vect_height_2d.push_back(cv::Point2f(prop.height, 0.0));
	cv::perspectiveTransform(vect_height_2d/*src*/, vect_height_3d/*dst*/, (matrix.inv()));

	if (vect_height_2d.size() == 2) {
		double height = abs(vect_height_3d[0].x - vect_height_3d[1].x);

		cm = centerOfMass(contour);
		vector_h = cv::Point2f(height, 0.0);
		double dx = h_vpoint.x - cm.x;
		double dy = h_vpoint.y - cm.y;
		double angel = (std::atan((double(dy / dx))));

		if (dx < 0)
			angel =  CV_PI + angel;

		vector_h = rotate(angel, cv::Point2f(0.0,0.0), vector_h);
		move(vector_h, contour);


		//cout << "cm: " << cm << endl;
		//cout << "va: " << vector_h << endl;
		//cout << "a: " << angel * 180.0/CV_PI << endl;
		//cout << "double(dy / dx )" << double(dy / dx) << endl;
		//cout << "dy  dx " << dy << " " << dx << endl;
	};

	Przedmiot3D::setExpirienced3D(contour);
}
// =============== scale =======================================

void Klocek::scaleDefinition(const double & scale) {
	PrzedmiotSceny::scaleDefinition(scale);
}

// ============== draw ===========================================

void Klocek::drawFound(cv::Mat & background) const
{
	PrzedmiotSceny::drawFound(background, _definition.color, CV_FILLED);
	PrzedmiotSceny::drawFound(background, cv::Scalar(0, 0, 0), 1);
}

void Klocek::draw2D(cv::Mat & background) const
{
	PrzedmiotSceny::draw2D(background);
}

void Klocek::draw3D(cv::Mat & background) const
{
	pobr::dot(background, h_vpoint);
	pobr::dot(background, cm);
	PrzedmiotSceny::draw3D(background);
}

void Klocek::drawDefined(cv::Mat & background) const
{
	PrzedmiotSceny::drawDefined(background);
}


