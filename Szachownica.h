#pragma once
#include "opencv2\opencv.hpp"
#include "Reactor.h"
#include "PrzedmiotSceny.h"
#include "pobr_types.h"




using namespace std;
using namespace cv;


class Szachownica:
	protected PrzedmiotSceny
{
public:


private:
	//! mo�liwe odchylenie od pionu linii pionowych
	int _alpha_v; 

	//! mo�liwe odchylenie od poziomu linii poziomych
	int _alpha_h;

	//! czy definicja zosta�a uzyskana na podstawie obserwacji
	bool _is_definision_from_exp;

	cv::Size _findSizeOfScene(const lines_t& hought_lines);
	bool _findContour3D_Exp();
	bool _findDefinition();
	bool _findMatrix3DTo2D();



protected:

	lines_t _vlines;
	lines_t _hlines;

	static void find_best_Vlines(lines_t& vlines, const cv::Size& size);
	static void find_best_Hlines(lines_t& hlines, const lines_t& vlines, cv::Mat& warpMatrix, const cv::Size& size);


public:
	//================= S T A T I C =================
	static void __find_Chessboard_lines(const lines_t& src_lines, lines_t& dst_vlines, lines_t& dst_hlines, cv::Mat& warpMatrix,
								  const cv::Size& size, const double& alpha_v = 25, const double alpha_h = 1);

	static cv::Mat __draw_Chessboard(const lines_t& vlines, const lines_t& hlines, const cv::Mat& background);
	//==============================================

	bool getMatrix3DTo2D(cv::Mat& matrix) const;
	void setChessboardLines(const lines_t& hought_lines);

	void setChessboardLines(Reactor & zdjecie);
	
	bool find_Chessboard();
	bool find_Chessboard(const lines_t& hought_lines);

	bool find_Chessboard(Reactor & zdjecie);

	void scaleDefinition(const double & scale);
	void drawFound(cv::Mat& background) const;
	void draw2D(cv::Mat& background) const;
	void draw3D(cv::Mat& background) const;
	void drawDefined(cv::Mat& background) const;

	Szachownica(const lines_t& hought_lines);
	Szachownica(Reactor& zdjecie);
	~Szachownica();
};

