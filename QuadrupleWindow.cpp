#include "QuadrupleWindow.h"



void QuadrupleWindow::do_callback()
{
	try {
		do_Quadruple_callback();
		show_screen();
	}
	catch (std::exception e) {
		std::cout << e.what() << std::endl;
	};
}

void QuadrupleWindow::show_screen()
{
	try {
		cv::Mat line_1, line_2;
		cv::hconcat(screen[0], screen[1], line_1);
		cv::hconcat(screen[2], screen[3], line_2);
		cv::vconcat(line_1, line_2, line_1);
		cv::imshow(_name, line_1);
	}
	catch (std::exception e) {
		std::cout << e.what() << std::endl;
	};
}

