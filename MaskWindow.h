#pragma once
#include "MyWindow.h"
#include "Mask.h"
#include "QuadrupleWindow.h"
#include "MaskReactor.h"

class MaskWindow : public QuadrupleWindow
{
	// Inherited via QuadrupleWindow
	virtual void do_Quadruple_callback() override;
	void onWindowsChanged();
	void set_MaskSettings_From_Trackbars();
	void set_Trackbars_From_MaskSettings();

	

	void refreshScreeen(const bool& all = false);
	void regenerateMasks(const bool& all = false);

	void _regenerateMasks(int screen_nr);
	void _refreshScreeen(int screen_nr);

	bool setMask_s(const int & i, const HSV_setings & mask_s);
	bool getMask_s(const int& i,  HSV_setings & mask_s);
	

	const cv::Mat getImage() { 
		Reactor* img_r = (Reactor *)(_img);
		return   img_r->getResult();
	};

	int _last_mask_index;

	vector<HSV_setings> _masks_s;
	MaskReactor* _img_r;
	
public:
	MaskWindow(MaskReactor& img);


};

