#pragma once
#ifndef POBR_TYPES_H_
#define POBR_TYPES_H_

#include "opencv2\opencv.hpp"
//#include "Mask.h"
#include <cstdint>
#include <cstdint>
#include <intrin.h>
#include <stdint.h>


class Mask;
typedef std::vector< Mask > masks_t;

typedef std::vector< cv::Point2f >  contour2f_t;
typedef std::vector< cv::Point >    contour_t;
struct Przedmiot_properties {
	contour2f_t contour2D;
	double height;
	cv::Scalar color;
};


typedef std::vector< std::vector< cv::Point > > contours_t;
typedef std::vector< cv::Vec4i >                hierarchy_t;
struct contours_h_t
{
	contours_t contours; 
	hierarchy_t hierarchy;
};
typedef std::vector< contours_h_t >             contours_F_t;

//! linie szachownicy

typedef std::vector<cv::Mat>               masked_t;
typedef std::vector < cv::Vec4i >          lines_t;



struct HSV_setings {
	int min_h;
	int max_h;

	int min_s;
	int max_s;

	int min_v;
	int max_v;
};

enum class Mask_type { RED, GREEN, CYLINDER, WHITE_SQUARES, BLACK_SQUARES, BACKGROUND };

struct Canny_setings {
	double canny_thr1;
	double canny_thr2;
};
struct Hough_setings {
	double hough_r0;
	double hough_theta;
	int    hough_thr;
	double hough_minLength;
	double hough_maxGap;
	int	   x_factor;
	int	   y_factor;
};
struct Erosion_setings {
	int size;
	int type;
};
struct Dilation_setings {
	int size;
	int type;
};

extern contour2f_t testContours;
extern contour2f_t czerwonyKlocekContour;
extern Przedmiot_properties testProperties;
extern Przedmiot_properties szachownica_properties;
extern Przedmiot_properties zielonyKlocekProperties;
extern Przedmiot_properties czerwonyKlocekProperties;
extern Przedmiot_properties walecProperties;
extern cv::Point2f h_vpoint;



#endif /* POBR_TYPES_H_*/