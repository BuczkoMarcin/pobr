#pragma once
#include "Szachownica.h"
#include "MyWindow.h"
#include "Reactor.h"
#include "Przedmiot2D.h"

class SzachownicaWindow: public MyWindow
{
	cv::Mat _img_szachy;
	bool _show_3dContours;
	bool _show_org_img;
	bool _show_2dContours;
	bool _show_Found;

	void set_flags_on_callback();

	Przedmiot2D P;

public:
	SzachownicaWindow(Reactor& img);
	~SzachownicaWindow();

	// Inherited via MyWindow
	virtual void do_callback() override;
	virtual void onWindowsChanged() override;
};

