#pragma once

#ifndef UTILS_H_
#define UTILS_H_

#include "opencv2\calib3d\calib3d.hpp"
#include "opencv2\flann\flann.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"


using namespace cv;
using namespace std;


namespace pobr {

	extern RNG rng;

	vector<Vec4i> prun_verticla_lines(const vector<Vec4i>& vlines);
	int partition_lines(const vector<Vec4i>& lines, map< int, vector<Vec4i> >& labeled_lines, const double& max_distance_ = 5.0, const double& max_apha_ = 3.0);
	void perspectiveTransform(const vector<Vec4i>& src_lines, vector<Vec4i>& dest_lines, InputArray m);

	struct line_ab {
		double a = 0;
		double b = 0;
		double vert_x = 0;
		bool perfect_vertical = false;
	};

	void print(const line_ab& lab);

	void scaleYline(Vec4i& line, const double& scale_, const int& Yp);

	void scaleYlines(vector<Vec4i>& lines, const double& scale, const int& Yp);

	void lines(Mat& mat, const vector<Vec4i>& lines, const Scalar& color = Scalar(255, 255, 255), int thickness = 1, int lineType = 8);

	void line(Mat& mat, const Vec4i& l, const Scalar& color = Scalar(255, 255, 255), int a = 1, int b = 8);

	void dot(Mat& mat, const Point p, const Scalar& color = Scalar(255, 255, 255));

	void transform_ab(const Vec4i& line, line_ab& lineab);

	void transform_vec(const line_ab& l, Vec4i& v);

	bool is_verticla_board_line(const Vec4i& line, const int& alpha);

	bool is_horizontal_board_line(const Vec4i& line, const double& alpha);

	void extend_line(Vec4i& line, const double& max_x_, const double& max_y_, const int& margin = 0);

	void extend_lines(vector<Vec4i>& lines, const double& max_x_, const double& max_y_, const int& margin = 0);

	bool crosspoint(const line_ab& L, const line_ab& K, Point& crosspoint_);
	bool crosspoint(const Vec4i& L, const Vec4i& K, Point& crosspoint_);

	//!por�wnywanie linii unormowanych (kat nachylenia + odleglosci od srodkow linii) 
	bool isEqual_v(const Vec4i& _l1, const Vec4i& _l2, double& distance_m, const int& max_distance_, const double& max_apha_);
	bool isEqual_p(const Point& p1, const Point& p2, double max_distance);

	Point center(const Vec4i& line);

	void transform_center_to(const Point&  p, Vec4i&  l);

	Vec4i normal_line(const Vec4i& line);

	//! bez k�t�w prostych mi�dzy liniami - ten przypadek nie jest rozpatrywany
	Vec4i average(const vector<Vec4i>& lines);

	//! �rednie ze wszystkich polabelowanych linii
	vector<Vec4i> average(map< int, vector<Vec4i> >& label_lines);

	vector<Vec4i> group_average(const vector<Vec4i>& lines, const int& cols, const int& rows, const double& max_distance = 5.0, const double& max_alpha = 3.0);

	int partition_points(const vector<Point>& crosspoints, vector<int>& labels, double max_distance);
	//! grupuje linie ze wzgl�du na pasowanie do siebie (odleglosc �rodk�w i k�t)
	int partition_lines(const vector<Vec4i>& lines, map< int, vector<Vec4i> >& labeled_lines, const double& max_distance_, const double& max_apha_);

	void sort_verticla_lines(vector<Vec4i>& vlines);

	void sort_horizontal_lines(vector<Vec4i>& hlines);

	map<int, Point> all_crosspoints(vector<Vec4i>& lines, Vec4i& l);

	void find_all_corsspoints(const vector<Vec4i>& lines, vector<Point>& crosspoints, vector<pair<int, int>>&  lines_idexes);

	Point average(const vector<Point>& points);

	//! zwraca linie kt�re wychodz� z jednego punktu zbiegu. Zwraca najwi�kszy taki podzbi�r linii. 
	vector<Vec4i> prun_verticla_lines(const vector<Vec4i>& vlines, const int& cols, const int& rows);

	vector<Vec4i> vertical_lines_from_Vanishing(const Point& vp, const int& l_numer, const double& dx = 0, const double& da = 0, const int& cols = 100, const int& rows = 100);

	vector<Vec4i> horizontal_lines_inPerspective(Mat& warpMatrix, const int& l_number, const Point& vp, const Point& lb, const Point& rb, const Point& ru, const Point& lu);
	vector<Vec4i> horizontal_lines_inPerspective(Mat& warpMatrix, const int& l_number, const Point& vp, const Point& lb, const Point& ru);

	Point VanishigP(vector<Vec4i>& vlines);

	vector<Vec4i>  numebrOfEqualLines(const vector<Vec4i>& L1, const vector<Vec4i>& L2, double& distance_m, int& numebrOfEquals, const double& max_distance = 5, const double& max_alpha = 3);

	vector<Vec4i> find_chessboard_vectical_lines(vector<Vec4i>& vlines, const int& cols, const int& rows,
		const int& marginx_ = 15, const int& margina_ = 100, const int& stepx = 1, const int& stepa = 3);

	void perspectiveTransform(const vector<Vec4i>& src_lines, vector<Vec4i>& dest_lines, InputArray m);

	vector<Vec4i> find_chessboard_horizontal_lines(vector<Vec4i>& hlines, const vector<Vec4i>& vlines, Mat& warpMatrix, const int& cols, const int& rows);

};

#endif
