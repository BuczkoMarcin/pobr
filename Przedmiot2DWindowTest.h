#pragma once
#include "Szachownica.h"
#include "MyWindow.h"
#include "FiguryFinder.h"
#include "Przedmiot2D.h"


class Przedmiot2DWindowTest: public MyWindow
{
	cv::Mat _img_szachy;
	bool _show_Defined;
	bool _show_org_img;
	bool _show_2dContours;
	bool _show_Found;
	double _angle;
	int _x, _y;
	int _mask_i;

	void set_flags_on_callback();

	Przedmiot2D P;
	Przedmiot2D P1;
	Przedmiot2D P2;
	FiguryFinder* _img_F;
	contour2f_t contour;
	contour2f_t contour1;
	contour2f_t contour2;

public:
	Przedmiot2DWindowTest(FiguryFinder& img);
	~Przedmiot2DWindowTest();

	// Inherited via MyWindow
	virtual void do_callback() override;
	virtual void onWindowsChanged() override;
};

