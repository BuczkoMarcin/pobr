#include "Reactor.h"




void Reactor::reset()
{
	_result = _src.clone();
}

void Reactor::IlluminationNormalization() {
	cv::Mat _r = _result.clone();
	cv::cvtColor(_r, _r, CV_BGR2GRAY);

	GaussianBlur(_r, _r, cv::Size(201, 201), 10, 10);
	//GaussianBlur(_r, _r, cv::Size(201, 201), 10, 10);
	//GaussianBlur(_r, _r, cv::Size(201, 201), 10, 10);
	bitwise_not(_r, _r);
	cv::cvtColor(_r, _r, CV_GRAY2BGR);
	cv::Scalar s = cv::mean(_r);
	
	cv::cvtColor(_r, _r, CV_BGR2HSV);
	cv::cvtColor(_result, _result, CV_BGR2HSV);

	cv::Mat_<cv::Vec3b> R = _result;
	cv::Mat_<cv::Vec3b> r = _r;

	for (int i = 0; i < r.rows; ++i)
		for (int j = 0; j < r.cols; ++j) {
			if (0.5*R(i, j)[2] + 0.5*r(i, j)[2] < 0)
				R(i, j)[2] = 0;
			else if (0.5*R(i, j)[2] + 0.5*r(i, j)[2] > 255)
				R(i, j)[2] = 255;
			else
				R(i, j)[2] = 0.5*R(i, j)[2] + 0.5*r(i, j)[2];
		}
	_r = r;
	_result = R;
	cv::cvtColor(_result, _result, CV_HSV2BGR);
	cv::cvtColor(_r, _r, CV_HSV2BGR);
}


const cv::Mat Reactor::doFilter(const bool& normalizeIlumination)
{
	_result = __doFilter(_result);
	if(normalizeIlumination)
		IlluminationNormalization();
	GaussianBlur(_result, _result, cv::Size(9, 9), 2, 2);
	

	return _result;
}

void Reactor::IlluminationCorrection() {

		cv::cvtColor(_result, _result, CV_BGR2Lab);

		std::vector<cv::Mat> lab_planes(3);
		cv::split(_result, lab_planes); 

		cv::Ptr<cv::CLAHE> clahe = cv::createCLAHE();
		clahe->setClipLimit(1);
		cv::Mat dst;
		clahe->apply(lab_planes[0], dst);

		dst.copyTo(lab_planes[0]);
		cv::merge(lab_planes, _result);
		cv::cvtColor(_result, _result, CV_Lab2BGR);

}


const cv::Mat Reactor::doCanny()
{
	_result = __doCanny(_result);
	cv::cvtColor(_result, _result, CV_GRAY2BGR);
	return _result;
}

const cv::Mat Reactor::doHough(lines_t & lines)
{
	cv::cvtColor(_result, _result, CV_BGR2GRAY);
	lines = __doHough(_result);
	cv::cvtColor(_result, _result, CV_GRAY2BGR);
	//return _result;
	return __doHoughImg(lines, _result);
}

const cv::Mat Reactor::doHough()
{
	lines_t lines;
	return doHough(lines);
}

const cv::Mat Reactor::getResult()
{
	return _result.clone();
}

void Reactor::resultShow(const std::string & window_name)
{
	cv::imshow(window_name, _result);
}
