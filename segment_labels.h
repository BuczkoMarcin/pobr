#pragma once
#include "opencv2/core/core.hpp"
#include "opencv2/highgui/highgui.hpp"
#include <iostream>
#include <list>
#include <map>

class Segment_labels
{
	
public:
	typedef int t_Label;

	Segment_labels(const cv::Mat& picture);
	~Segment_labels();
	t_Label& operator() (const int& x, const int& y);

	static const t_Label _bad_label = -1;
	static const t_Label _null_label = 0;

private:
	typedef std::vector<std::vector<t_Label>> t_Label_array;
	typedef int t_Label_arra_lenght;
	t_Label_array _labels;
	t_Label_arra_lenght _lenght_x, _lenght_y;
};

