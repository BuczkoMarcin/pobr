#pragma once
#ifndef MASK_H_
#define MASK_H_

#include "opencv2\opencv.hpp"
#include "Zdjecie.h"
#include "pobr_types.h"
#include <cstdint>
#include <intrin.h>
#include <stdint.h>





class Mask
{
public:

	static const HSV_setings RED;
	static const HSV_setings GREEN;
	static const HSV_setings CYLINDER;
	static const HSV_setings WHITE_SQUARES;
	static const HSV_setings BLACK_SQUARES;
	static const HSV_setings BACKGROUND;
	static const HSV_setings NULL_MASK;

private:
	std::map<Mask_type, HSV_setings> _mask_setings;

	void generateMask(const cv::Mat& full_img, const Mask_type& type);
	void generateMask(const cv::Mat& full_img, const HSV_setings& m_settings);
	void filterMask();
	void init();

protected:
	//cv::Mat full_img;
	cv::Mat _mask;

public:
	Mask(const cv::Mat& img, const Mask_type& type);
	Mask(const cv::Mat& img, const HSV_setings& m_settings);

	Mask() {  init(); };
	Mask(const Mask &m) {  m.getMask(_mask); init(); };
	Mask& operator=(const Mask &m) { m.getMask(_mask); init(); return *this; };

	~Mask();


	void getMask(cv::Mat& mask_dst) const;
	void showOn(cv::Mat&);

	void reGenerateMask(const cv::Mat& full_img, const HSV_setings& m_settings);
	void reGenerateMask(const cv::Mat& full_img, const Mask_type& type);

};

#endif 

