#include "Przedmiot2D.h"
#include "contours_utils.h"
#include "pobr_types.h"


const Przedmiot_properties Przedmiot2D::default_properties{
	{ cv::Point2f(0,200.0),
	cv::Point2f(200.0,200.0),
	cv::Point2f(200.0,0.0),
	cv::Point2f(0.0,0.0) },
	200.0,
	cv::Scalar(255,255,255)
};



void Przedmiot2D::reset()
{
	_is_found = false;
	_is_expirienced2D = false;
}

// ============== find ===========================================

bool Przedmiot2D::findPrzedmiot() {

	if (!_is_expirienced2D)
		return false;

	if (_is_found)
		return true;

	cv::Point2f translation;
	double orientation;

	findTranslation(translation);
	findRotation(orientation);
	_contour_found = _definition.contour2D;
	move(translation, _contour_found);

	cv::Point2f cm = centerOfMass(_contour_found);
	rotate(orientation, cm, _contour_found);

	_is_found = true;

	return true;
}

bool Przedmiot2D::findPrzedmiot(contour2f_t & contour_found)
{
	if (!findPrzedmiot())
		return false;
	contour_found = _contour_found;
	return true;
}

bool Przedmiot2D::findTranslation(cv::Point2f & translation) const
{
	if (!_is_expirienced2D)
		return false;
	translation = centerOfMass(_contour2D_exp) - centerOfMass(_definition.contour2D);
	return true;
}

bool Przedmiot2D::findRotation(double & rotation) const
{

	cv::RotatedRect bounding_rect;
	cv::RotatedRect bounding_elipse;

	cv::RotatedRect bounding_rect_def;
	cv::RotatedRect bounding_elipse_def;

	bounding_rect.angle = 0.0;;
	bounding_elipse.angle = 0.0;;

	bounding_rect_def.angle = 0.0;;
	bounding_elipse_def.angle = 0.0;

	bounding_rect = cv::minAreaRect(_contour2D_exp);
	bounding_rect_def = cv::minAreaRect(_definition.contour2D);
	double rotation_rect = bounding_rect.angle - bounding_rect_def.angle;

	if (_contour2D_exp.size() < 5 || _definition.contour2D.size() < 5) {
		if (bounding_rect.size.width < bounding_rect.size.height)
			rotation_rect += 90;
		rotation = rotation_rect*CV_PI/180.0;
		return true;
	};

	//rotation = rotation_rect*CV_PI / 180.0;
	//return true;

	//// ellipse
	//cv::ellipse(drawing, minEllipse[i], color, 2, 8);
	//// rotated rectangle
	//cv::Point2f rect_points[4]; minRect[i].points(rect_points);
	//for (int j = 0; j < 4; j++)
	//	line(drawing, rect_points[j], rect_points[(j + 1) % 4], color, 1, 8);




	bounding_elipse = cv::fitEllipse(_contour2D_exp);
	bounding_elipse_def = cv::fitEllipse(_definition.contour2D);

	double rotation_elipse = bounding_elipse.angle - bounding_elipse_def.angle;
	rotation = (CV_PI / 180.0) * (rotation_rect + rotation_elipse) / 2.0;

	return true;
}

// ============= get set =====================================

void Przedmiot2D::setExpirienced2D(const contour2f_t & contour_exp)
{
	reset();
	_contour2D_exp = contour_exp;
	_is_expirienced2D = true;
}

void Przedmiot2D::setDefinition(const Przedmiot_properties & definition)
{
	_definition = definition;
	_is_found = false;
}

bool Przedmiot2D::getExpirienced2D(contour2f_t & contour_exp)
{
	if (!_is_expirienced2D)
		return false;
	contour_exp = _contour2D_exp;
	return true;
}

bool Przedmiot2D::getDefinition(Przedmiot_properties & definition)
{
	if (!(_definition.contour2D.size() > 0))
		return false;
	definition = _definition;
	return true;
}

bool Przedmiot2D::getFound(contour2f_t & contour_found)
{
	return findPrzedmiot(contour_found);
}

void Przedmiot2D::scaleDefinition(const double & scale)
{
	reset();
	scaleConture(_definition.contour2D, scale);
	_definition.height *= scale;
};

// ============ Draw =====================================

void Przedmiot2D::drawDefined(cv::Mat & backgroung, const cv::Scalar& color, const int& thickness) const
{
	drawContour2f(backgroung, _definition.contour2D, color, thickness);
}

void Przedmiot2D::draw2D(cv::Mat & backgroung) const
{
	if (_is_expirienced2D)
		drawContour2f(backgroung, _contour2D_exp, cv::Scalar(255, 127, 127));
}

void Przedmiot2D::drawFound(cv::Mat & backgroung, const cv::Scalar& color, const int& thickness) const
{
	if (_is_found)
		drawContour2f(backgroung, _contour_found, color, thickness);
}

//=========== Constructor ================================

Przedmiot2D::Przedmiot2D(const Przedmiot_properties & definition) :
	Przedmiot(), _is_found(false), _is_expirienced2D(false), _definition(definition) {}

Przedmiot2D::Przedmiot2D() : Przedmiot2D(default_properties) {}