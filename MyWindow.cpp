#include "MyWindow.h"
//#include "opencv2\opencv.hpp"
#include "opencv2\core\core.hpp"

MyWindow::all_windows_t MyWindow::all_windows = all_windows_t();


MyWindow::MyWindow(Zdjecie& img, const string& window_name, 
	               trackbar_vect_t& trackbars, const bool& external_trackbars ,const string& trackbar_window_name) :
	_trackbars(trackbars), _img(&img), _name(window_name)
{
	register_window();
	cv::namedWindow(_name, cv::WINDOW_NORMAL);
	cv::Size size = img.getSize();
	_img->imgShow(_name);
	cv::resizeWindow(_name, 800, 100+size.height * double(800.0 / size.width));
	string tr_w_name = _name; 
	if (external_trackbars) {
		tr_w_name = trackbar_window_name;
		cv::namedWindow(tr_w_name, cv::WINDOW_AUTOSIZE);
		cv::resizeWindow(tr_w_name, 800, 400);
	};

	for (trackbar_t& tr : _trackbars)
		cv::createTrackbar(tr.name, tr_w_name, &tr.value, tr.max_value, trackbar_callback, this);
	//cv::updateWindow("Trackbars");  // Break Error!!
	//trackbar_callback(0, this); // Break Error!! 
}

MyWindow::~MyWindow()
{
	unregister_window();
}

void MyWindow::trackbar_callback(int value, void* userdata) {
	MyWindow* _this = (MyWindow*)userdata;
	_this->do_callback();
	_this->_signal();
}

void MyWindow::register_window()
{
	vector<MyWindow*>* _v = &(all_windows[_img]);
	if (_v != nullptr) {
		for (auto window : *_v) {
			window->_signal.connect(boost::bind(&MyWindow::onWindowsChanged, this));
			_signal.connect(boost::bind(&MyWindow::onWindowsChanged, window));
		}
	};
	all_windows[_img].push_back(this);
	//cout << "register_window: " << endl << all_windows;
}

void MyWindow::unregister_window()
{

	_signal.disconnect_all_slots();
	vector<MyWindow*>* _v = &(all_windows[_img]);
	if (_v != nullptr) {
		//cout << "unregister_window begin: " << endl << all_windows;
		_v->erase(std::remove(_v->begin(), _v->end(), this), _v->end());
		if (_v->size() == 0)
			all_windows.erase(_img);
		//cout << "unregister_window end: " << endl << all_windows;
	};
}

ostream & operator<<(ostream & os, MyWindow::all_windows_t & aw)
{

	for (auto &item : aw) {
		os << item.first << " => " << item.second << endl;
	}
	return os;
}

ostream & operator<<(ostream & os, MyWindow::windows_t & aw)
{
	for (auto item : aw) {
		os << item << ", ";
	}
	return os;
};
