#pragma once
#include "MyWindow.h"
class QuadrupleWindow :
	public MyWindow
{
	vector<cv::Mat> screen;
	virtual void do_callback() override;

protected:
	virtual void do_Quadruple_callback() = 0;
	void show_screen();
	void setScreen_1(const cv::Mat& screen_) { screen[0] = screen_; };
	void setScreen_2(const cv::Mat& screen_) { screen[1] = screen_; };
	void setScreen_3(const cv::Mat& screen_) { screen[2] = screen_; };
	void setScreen_4(const cv::Mat& screen_) { screen[3] = screen_; };
public:
	QuadrupleWindow(Zdjecie& img, const string& window_name,
		trackbar_vect_t& trackbars, const bool& external_trackbars=false, const string& trackbar_window_name="Trackbars") :
		MyWindow(img, window_name, trackbars, external_trackbars, trackbar_window_name) {
		screen.push_back(_img->getImage());
		screen.push_back(_img->getImage());
		screen.push_back(_img->getImage());
		screen.push_back(_img->getImage());
		show_screen();
	};
};

