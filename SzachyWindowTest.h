#pragma once
#include "MyWindow.h"
#include "Szachownica.h"
#include "FiguryFinder.h"
#include "Klocek.h"
#include "Walec.h"

class SzachyWindowTest :
	public MyWindow
{
	cv::Mat _img_szachy;
	bool _show_3dContours;
	bool _show_org_img;
	bool _show_2dContours;
	bool _show_Found;
	bool _show_definition;
	bool _show_h_lines;

	lines_t h_lines;
	Szachownica s;
	Klocek zielony;
	Klocek czerwony;
	Walec walec;
	FiguryFinder *f;
	cv::Mat matrix3Dto2D;
	bool _is_matrix;

public:
	SzachyWindowTest(FiguryFinder& img);
	~SzachyWindowTest();

	// Inherited via MyWindow
	virtual void do_callback() override;
	virtual void onWindowsChanged() override;
	void findWalec();
	void findKlocek(Klocek & k, const int & i);
	void findSzachownica();
	void set_flags_on_callback();
};

