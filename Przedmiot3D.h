#pragma once
#include "Przedmiot.hpp"
class Przedmiot3D :
	public Przedmiot
{
	// contour obiektu jaki zosta� uzyskany ze zdj�cia
	contour2f_t    _contour3D_exp;

	// czy obiekt ma contour uzyskany ze zdj�cia
	bool _is_expirienced3D;

protected:
	void reset();
	bool is_expirienced3D() {
		return _is_expirienced3D;
	}

public:
	// ustawia contour uzyskany ze zdj�cia
	void setExpirienced3D(const contour2f_t&  contour_exp);
	
	// zwraca contour uzyskany ze zdj�cia
	bool getExpirienced3D(contour2f_t& contour_exp);

	Przedmiot3D();
	Przedmiot3D(const contour2f_t& _contour3D_exp);

	void draw3D(cv::Mat& backgroung) const;
};

