#include "Przedmiot3D.h"
#include "contours_utils.h"



void Przedmiot3D::reset()
{
	_is_expirienced3D = false;
}

void Przedmiot3D::setExpirienced3D(const contour2f_t & contour_exp)
{
	_contour3D_exp = contour_exp;
	_is_expirienced3D = true;
}

bool Przedmiot3D::getExpirienced3D(contour2f_t & contour_exp)
{
	if (!_is_expirienced3D)
		return false;
	contour_exp = _contour3D_exp;
	return true;
}

Przedmiot3D::Przedmiot3D()
{
	reset();
}

Przedmiot3D::Przedmiot3D(const contour2f_t & contour3D_exp)
{
	setExpirienced3D(contour3D_exp);
}

void Przedmiot3D::draw3D(cv::Mat & img) const
{
	if (_is_expirienced3D)
		drawContour2f(img, _contour3D_exp, cv::Scalar(127, 255, 127),3);
}
