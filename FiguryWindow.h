#pragma once
#include "QuadrupleWindow.h"
#include "FiguryFinder.h"

class FiguryWindow :
	public QuadrupleWindow
{
	// Inherited via QuadrupleWindow
	virtual void onWindowsChanged() override;
	virtual void do_Quadruple_callback() override;
	
	void refreshScreeen_F(const bool& all = false);
	void _refreshScreeen_F(int screen_nr);

	FiguryFinder* _img_F;
public:
	FiguryWindow(FiguryFinder &img) :
		QuadrupleWindow(img, "Figury", trackbar_vect_t({
			{ 0,255,"P" }
	})), _img_F(&img) {};
};

