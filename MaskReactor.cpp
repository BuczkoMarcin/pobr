#include "MaskReactor.h"


const cv::Mat MaskReactor::__doMask(const Mask & mask_, const cv::Mat & src)
{
	cv::Mat masked;
	cv::Mat mask;
	mask_.getMask(mask);
	src.copyTo(masked, mask);
	return masked;
}

const cv::Mat MaskReactor::_doMask(const int & i)
{
	if (is_index_masked_ok(i) && is_index_masks_ok(i)) {
		_masked[i] = __doMask(_masks[i], _result);
		return _masked[i];
	}
	return cv::Mat();
}

const cv::Mat MaskReactor::_doMask(const int & i, const Mask & mask_)
{
	if (is_index_masked_ok(i) && is_index_masks_ok(i)) {
		_masks[i] = mask_;
		return _doMask(i);
	}
	return cv::Mat();
}

const cv::Mat MaskReactor::_doMask(const int & i, const HSV_setings & mask_s)
{
	if (is_index_masked_ok(i) && is_index_masks_ok(i)) {
		_masks[i].reGenerateMask(_result, mask_s);
		return _doMask(i);
	}
	return cv::Mat();
}

const masked_t MaskReactor::doMask()
{
	_masked.clear();
	for (size_t i = 0; i < _masks.size(); ++i) {
		_masked.push_back(cv::Mat(_result.size(),_result.type()));
		_doMask(i);
	}
	return _masked;
}

const masked_t MaskReactor::doMask(const masks_t& masks)
{
	_masked.clear();
	_masks.clear();
	_masks = masks;
	doMask();
	return _masked;
}

const masked_t MaskReactor::doMask(const std::vector<HSV_setings>& masks_s)
{
	masks_t masks;
	for(HSV_setings ms : masks_s) {
		masks.push_back(Mask(_result, ms));
	}
	doMask(masks);
	return _masked;
}

bool MaskReactor::getMask(const int& i, Mask& dst)
{
	if (!is_index_masks_ok(i))
		return false;
	dst = _masks[i];
	return true;
}

bool MaskReactor::getMasked(const int& i, cv::Mat& dst)
{
	if (!is_index_masked_ok(i))
		return false;
	dst = _masked[i].clone();
	return true;
}

