#include "Walec.h"
#include "PrzedmiotSceny.h"
#include "contours_utils.h"



void Walec::findDefinition() {
	Przedmiot_properties prop = _definition;
	if (prop.height!=0) {
		double radius = prop.height;
		double da = (CV_PI / 6);
		prop.contour2D.clear();
		for (double i = 0; i <= 2 * CV_PI; i += da) {
			prop.contour2D.push_back(cv::Point2f(radius+radius*std::sin(i), radius+radius*std::cos(i)));
		}
		setDefinition(prop);
	}
}

Walec::Walec(const Przedmiot_properties& definition) :
	Klocek(definition)
{
	findDefinition();
}

Walec::Walec() :
	Walec(walecProperties)
{
}

Walec::Walec(const contour2f_t & contour3d_exp, const cv::Mat & matrix3DTo2D) :
	Walec(walecProperties)
{
	setMatrix3DTo2D(matrix3DTo2D);
	setExpirienced3D(contour3d_exp);
}

Walec::~Walec()
{
}


bool Walec::findPrzedmiot() {
	if (!Klocek::findPrzedmiot())
		return false;
	contour2f_t contour;
	if(!getFound(contour))
		return false;
	Przedmiot_properties prop;
	if (!getDefinition(prop))
		return false;
	move(cv::Point2f(0.0, -prop.height), contour );
	_contour_found = contour;
}

void Walec::setExpirienced3D(const contour2f_t & contour_exp) {

	
	cv::Rect brect = boundingRect(contour_exp);

	double y = brect.y + brect.height;
	double x1 = brect.x;
	double x2 = brect.x + brect.width;

	contour2f_t contour;
	contour.push_back(cv::Point2f(x1, y));
	contour.push_back(cv::Point2f(x2, y));

	Przedmiot_properties prop;
	cv::Mat matrix;
	if (!getDefinition(prop) || !getMatrix3DTo2D(matrix)) {
	
		Przedmiot3D::setExpirienced3D(contour);
		return;
	}

	Przedmiot3D::setExpirienced3D(contour);
}
