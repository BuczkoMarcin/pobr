#include "ReactorWindow.h"
#include "Szachownica.h"



void ReactorWindow::do_Quadruple_callback()
{
	try {
		Reactor* _img_r = (Reactor*)_img;
		_img_r->reset();
		_img_r->setFilter({       _trackbars[0].value, cv::MORPH_RECT },
		                  {       _trackbars[0].value, cv::MORPH_RECT });
		_img_r->setCanny({ double(_trackbars[1].value), 
						   double(_trackbars[2].value) });
		_img_r->setHough({ double(_trackbars[3].value),
						   double(_trackbars[4].value),
								  _trackbars[5].value,
						   double(_trackbars[6].value),
						   double(_trackbars[7].value),
								  1,
								  1 });
		lines_t hough_lines;
		lines_t hlines;
		lines_t vlines;
		
		
		setScreen_1(_img_r->doFilter());
		setScreen_2(_img_r->doCanny());
		setScreen_3(_img_r->doHough(hough_lines));
		cv::Mat matrix;
		Szachownica::__find_Chessboard_lines(hough_lines, vlines, hlines, matrix, _img_r->getSize());
		setScreen_4(Szachownica::__draw_Chessboard(vlines, hlines, _img_r->getImage()));
	
		// na koniec zostawiamy w _result tylko obraz przefiltrowany
		_img_r->reset();
		_img_r->doFilter(true);
	}
	catch (std::exception e) {
		std::cout << e.what() << std::endl;
	};
}

void ReactorWindow::onWindowsChanged()
{
}
