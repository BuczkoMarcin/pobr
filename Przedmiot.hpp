#pragma once
#include <string>
#include <vector>
#include "opencv2\opencv.hpp"
#include "pobr_types.h"

class Przedmiot
{
protected:

	std::string _name;

public:

	void setName(const std::string& name) { _name = name; };
	std::string getName() const { return _name; };

	Przedmiot() : _name("No name") {};
	~Przedmiot() {};
};

